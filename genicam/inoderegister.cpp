/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "inoderegister.h"
#include "inoderegister_p.h"
#include "genicamobjectbuilder.h"
#include "xmlhelper.h"
#include "iport.h"
#include "iinteger.h"
#include "intswissknife.h"

#include <QDebug>
#include <QStringList>

using namespace Jgv::GenICam::InodeRegister;

ObjectPrivate::ObjectPrivate(QSharedPointer<Jgv::GenICam::IPort::Interface> iport):Inode::ObjectPrivate(iport)
{}

Object::Object(ObjectPrivate &dd)
    : Inode::Object(dd)
{}

Object::~Object() = default;

void Object::prepare(InterfaceBuilder &builder, const XMLHelper &helper)
{
    Q_D(Object);

    // prepare l'inode
    Inode::Object::prepare(builder, helper);


    {
        for (auto const &prop: helper.getProperties( Properties::Address )) {
            auto ok { false };
            auto val = prop.toLongLong(&ok, 0);
            if (ok) {
                d->addresses.append(val);
            }
            else {
                qWarning("InodeRegister: %s failed to parse Address %s", qPrintable(featureName()), qPrintable(prop));
            }
        }
    } // Address


    {
        for (auto const &prop: helper.getProperties( Properties::pAddress )) {
            d->pAddresses.append(pValueInteger(builder.buildInode(prop)));
        }
    } // pAddress


    {
        for (auto const &helper: helper.getChildrenHelpers( Properties::IntSwissKnife )) {
            auto intSwissKnife = QSharedPointer<IntSwissKnife::Object>::create(d->iport);
            intSwissKnife->prepare(builder, helper);
            d->intSwissKnives.append(intSwissKnife);
        }
    } // IntSwissKnife


    {
        for (auto const &helper: helper.getChildrenHelpers(Properties::pIndex)) {
            pValueInteger inode = pValueInteger(builder.buildInode( helper.value() ) );
            auto offset = helper.getAttribute(Attributes::Offset);
            if (!offset.isEmpty()) {
                auto ok { false };
                auto val = offset.toLongLong(&ok, 0);
                if (ok) {
                    d->pIndexes.append( Index { inode, pValueInteger(val) } );
                }
            }
            else {
                offset = helper.getAttribute(Attributes::pOffset);
                if (!offset.isEmpty()) {
                    d->pIndexes.append( Index{ inode,  pValueInteger(builder.buildInode(offset)) } );
                }
                else {
                    // l'offset n'est pas déclaré, on le force à 1
                    d->pIndexes.append( Index{ inode,  pValueInteger(1) } );
                }
            }
        }
    } // pIndex


    {
        QString prop = helper.getProperty(Properties::Length);
        if (!prop.isEmpty()) {
            auto ok { false };
            auto length = prop.toLongLong(&ok, 0);
            if (ok) {
                d->pLength = pValueInteger(length);
            } else {
                qWarning("InodeRegister: %s failed to parse Length %s", qPrintable(featureName()), qPrintable(prop));
            }
        }
        else {
            prop = helper.getProperty(Properties::pLength);
            if (!prop.isEmpty()) {
                d->pLength = pValueInteger(builder.buildInode(prop));
            }
            else {
                qWarning("InodeRegister: %s no length defined", qPrintable(featureName()));
            }
        }
    } // length


    {
        for (auto const &prop: helper.getProperties( Properties::pInvalidator )) {
            auto inode = builder.buildInode(prop);
            auto p = inode.toStrongRef();
            if (p) {
                d->invalidatorsList.append(inode);
                // obtient le weakpointer sur this par le builder
                p->setInodeToInvalidate(builder.buildInode(d->featureName));
            }
        }
    } // invalidators


    {
        auto prop = helper.getProperty(Properties::PollingTime);
        if (!prop.isEmpty()) {
            auto ok { false };
            d->pollingTime = prop.toInt(&ok, 0);
            if (!ok) {
                qWarning("InodeRegister: %s failed to parse PollingTime %s", qPrintable(featureName()), qPrintable(prop));
            }
        }
    } // PollingTime


    // si ce n'est pas précisé, le registre est en bigEndian
    d->isLittleEndian = (helper.getProperty(Properties::Endianess) == Endianess::LittleEndian);

    // si la propriété AccesMode n'existe pas,
    // le registre est en accès complet.
    d->canRead = true;
    d->canWrite = true;
    QString accesMode = helper.getProperty(Properties::AccessMode);
    if (accesMode == Inode::AccessMode::ReadOnly) {
        d->canWrite = false;
    }
    else if (accesMode == Inode::AccessMode::WriteOnly) {
        d->canRead = false;
    }

    QString cachable = helper.getProperty(Properties::Cachable);
    // WriteThrough : une écriture provoque la mise à jour du cache (la lecture aussi)
    if (cachable == Cache::WriteThrough) {
        d->readUpdateCache = true;
        d->writeUpdateCache = true;
    }
    // WriteAround : seule une lecture provoque la mise à jour du cache
    else if (cachable == Cache::WriteAround) {
        d->readUpdateCache = true;
        d->writeUpdateCache = false;
    }
    // NoCache : fonctionnement sans cache.
    else if (cachable == Cache::NoCache) {
        d->readUpdateCache = false;
        d->writeUpdateCache = false;
    }

    d->pPort = helper.getProperty(Properties::pPort);
}

QStringList Object::getInvalidatorFeatureNames() const
{
    Q_D(const Object);

    QStringList invalidators;

    for (auto const &invalidator: d->invalidatorsList) {
        auto p = invalidator.toStrongRef();
        if (p) {
            invalidators.append(p->featureName());
        }
    }

    return invalidators;
}

int Object::getPollingTime() const
{
    Q_D(const Object);
    return d->pollingTime;
}

bool Object::isWritable() const
{
    Q_D(const Object);
    return d->canWrite;
}

uchar *Object::data()
{
    Q_D(Object);

    if (d->data.isEmpty()) {
        d->data.resize(d->pLength.value());
    }

    if (d->canRead) {
        d->iport->read(d->data.data(), address(), d->data.size());
    }
    return d->data.data();

}

qint64 Object::dataSize() const
{
    Q_D(const Object);

    return d->pLength.value();
}

void Object::updateData()
{
    Q_D(Object);
    if (d->canWrite && (!d->data.isEmpty())) {
        d->iport->write(d->data.data(), address(), d->data.size());
        // invalide les inodes listés
        for (auto const &inode: d->inodesToInvalidate) {
            auto p = inode.toStrongRef();
            if (p) {
                p->invalidate();
            }
        }
    }
}

qint64 Object::address() const
{
    Q_D(const Object);

    qint64 address = 0;

    {   // adress
        for (auto const &add: d->addresses) {
            address += add;
        }
    }

    {   // pAddress
        for (auto const &add: d->pAddresses) {
            address += add.value();
        }
    }

    {   // IntSwissKnife
        for (auto const &add: d->intSwissKnives) {
            address += add->getValue();
        }
    }

    {   // pIndex
        for (auto const &add: d->pIndexes) {
            address += add.value();
        }
    }

    return address;
}











/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQOPENGLPIPELINE_H
#define GVSPQOPENGLPIPELINE_H

#include "gvspmemoryallocator.h"
#include "pimpl.hpp"
#include "gvspqtdefinitions.h"

class QOpenGLContext;
class QOpenGLTexture;
class PixelTransform;
class GLVideoRenderer;

namespace Jgv {

namespace Gvsp {

struct Image;

namespace QtBackend {

class OpenGLPipelinePrivate;
class OpenGLPipeline final : public Gvsp::MemoryAllocator
{

public:
    explicit OpenGLPipeline();
    ~OpenGLPipeline() override ;

    MemoryBlock allocate(std::size_t size) noexcept override final;
    void destroy(MemoryBlock &block) noexcept override final;
    void push(Image &image) noexcept override final;

    QWeakPointer<ReceiverConfig> config() const;
    void setSharedContext(QOpenGLContext *context);
    void setVideoRender(QWeakPointer<GLVideoRenderer> renderer);

private:
    BASE_PIMPL(OpenGLPipeline)
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv


#endif // GVSPQOPENGLPIPELINE_H

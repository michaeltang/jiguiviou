/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtjpegrecorder.h"
#include "gvspqtjpegrecorder_p.h"

#include "gvspqtxmp.h"
#include "date-master/date.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions_4_2_Core>

#include <QFile>
#include <QDir>

#include <turbojpeg.h>
#include <thread>

using namespace Jgv::Gvsp::QtBackend;

namespace {

ParsedTimecode parseTimecode(quint64 timestamp) {
    std::chrono::system_clock::time_point tp;
    tp += std::chrono::nanoseconds(timestamp);

    using namespace date;

    auto const dp = floor<days>(tp);
    auto const ymd = year_month_day(dp);
    auto const year = ymd.year();
    auto const month = ymd.month();
    auto const day = ymd.day();
    auto left = tp-dp;
    auto const hour = std::chrono::duration_cast<std::chrono::hours>(left);
    left-=hour;
    auto const minute = std::chrono::duration_cast<std::chrono::minutes>(left);
    left-=minute;
    auto const second = std::chrono::duration_cast<std::chrono::seconds>(left);
    left-=second;
    auto const milli = std::chrono::duration_cast<std::chrono::milliseconds>(left);

    return ParsedTimecode { static_cast<int>(year), static_cast<unsigned>(month), static_cast<unsigned>(day),
                hour.count(), minute.count(), second.count(), milli.count() };
}

QString toFilename(const ParsedTimecode &timecode)
{
    return QString("%0-%1-%2@%3_%4_%5_%6.jpg")
            .arg(timecode.year,   4, 10, QLatin1Char('0'))
            .arg(timecode.month,  2, 10, QLatin1Char('0'))
            .arg(timecode.day,    2, 10, QLatin1Char('0'))
            .arg(timecode.hour,   2, 10, QLatin1Char('0'))
            .arg(timecode.minute, 2, 10, QLatin1Char('0'))
            .arg(timecode.second, 2, 10, QLatin1Char('0'))
            .arg(timecode.milli,  3, 10, QLatin1Char('0'));
}

#ifdef XMP
void compressGreyImage(uint32_t quality, uint32_t width, uint32_t height, const VtoXmp &metas, const QString &filename, const std::vector<std::shared_ptr<uint8_t>> buffers)
{
    tjhandle handle = tjInitCompress();
    if (handle == nullptr) {
        const char *err = tjGetErrorStr();
        qWarning("Failed to create Turbo Jpeg instance: %s", err);
        return;
    }

    unsigned long jpegSize = 0;
    unsigned char * buff = nullptr;

    if (0 == tjCompress2(handle, buffers[0].get(), static_cast<int>(width), 0, static_cast<int>(height), TJPF_GRAY, &buff, &jpegSize, TJSAMP_GRAY, static_cast<int>(quality), 0)) {
        VtoXmp::saveToFile(buff, jpegSize, filename, metas);
    }
    else {
        const char *err = tjGetErrorStr();
        qWarning("Failed to turbo compress monochrome image : %s", err);
    }

    tjDestroy(handle);
    tjFree(buff);
}

void compressYuvImage(uint32_t quality, uint32_t width, uint32_t height, const VtoXmp &metas, const QString &filename, const std::vector<std::shared_ptr<uint8_t>> buffers)
{
    tjhandle handle = tjInitCompress();
    if (handle == nullptr) {
        const char *err = tjGetErrorStr();
        qWarning("Failed to create Turbo Jpeg I420 instance: %s", err);
        return;
    }
    unsigned char * buff = nullptr;
    unsigned long jpegSize = 0;
    const unsigned char * srcPlanes[3];
    srcPlanes[0] = buffers[0].get();
    srcPlanes[1] = buffers[1].get();
    srcPlanes[2] = buffers[2].get();
    if (0 == tjCompressFromYUVPlanes(handle, srcPlanes, static_cast<int>(width), nullptr, static_cast<int>(height), TJSAMP_420, &buff, &jpegSize, static_cast<int>(quality), 0)) {
        VtoXmp::saveToFile(buff, jpegSize, filename, metas);
    }
    else {
        const char *err = tjGetErrorStr();
        qWarning("Failed to turbo compress I420 image : %s", err);
    }

    tjDestroy(handle);
    tjFree(buff);
}

}
#else
void compressGreyImage(uint32_t quality, uint32_t width, uint32_t height, const QString &filename, const std::vector<std::shared_ptr<uint8_t>> buffers)
{
    tjhandle handle = tjInitCompress();
    if (handle == nullptr) {
        const char *err = tjGetErrorStr();
        qWarning("Failed to create Turbo Jpeg instance: %s", err);
        return;
    }

    unsigned long jpegSize = 0;
    unsigned char * buff = nullptr;

    if (0 == tjCompress2(handle, buffers[0].get(), static_cast<int>(width), 0, static_cast<int>(height), TJPF_GRAY, &buff, &jpegSize, TJSAMP_GRAY, static_cast<int>(quality), 0)) {
        QFile f(filename);
        f.open(QIODevice::WriteOnly);
        f.write(reinterpret_cast<const char *>(buff), static_cast<qint64>(jpegSize));
        f.close();
    }
    else {
        const char *err = tjGetErrorStr();
        qWarning("Failed to turbo compress monochrome image : %s", err);
    }

    tjDestroy(handle);
    tjFree(buff);
}

void compressYuvImage(uint32_t quality, uint32_t width, uint32_t height, const QString &filename, const std::vector<std::shared_ptr<uint8_t>> buffers)
{
    tjhandle handle = tjInitCompress();
    if (handle == nullptr) {
        const char *err = tjGetErrorStr();
        qWarning("Failed to create Turbo Jpeg I420 instance: %s", err);
        return;
    }
    unsigned char * buff = nullptr;
    unsigned long jpegSize = 0;
    const unsigned char * srcPlanes[3];
    srcPlanes[0] = buffers[0].get();
    srcPlanes[1] = buffers[1].get();
    srcPlanes[2] = buffers[2].get();
    if (0 == tjCompressFromYUVPlanes(handle, srcPlanes, static_cast<int>(width), nullptr, static_cast<int>(height), TJSAMP_420, &buff, &jpegSize, static_cast<int>(quality), 0)) {
        QFile f(filename);
        f.open(QIODevice::WriteOnly);
        f.write(reinterpret_cast<const char *>(buff), static_cast<qint64>(jpegSize));
        f.close();
    }
    else {
        const char *err = tjGetErrorStr();
        qWarning("Failed to turbo compress I420 image : %s", err);
    }

    tjDestroy(handle);
    tjFree(buff);
}
#endif

}




void JpegRecorderPrivate::renderAsGrey(const GLQuadRendering &quad, const PboMetas &metas)
{
    Q_UNUSED(quad)

    // on rejette les images sans date
    if (metas.timestamp == 0) {
        qDebug("Jpeg recorder failed to save 0 timestamp image");
        return;
    }


    const ParsedTimecode parsed = parseTimecode(metas.timestamp);
    const QString filename = config->jpegEffectiveRecordingDir().filePath(toFilename(parsed));

    // on rejette les images en double
    if (QFile::exists(filename)) {
        qDebug("Jpeg recorder failed to save, image exist");
        return;
    }

#ifdef XMP
    VtoXmp xmp { config->jpegXmp() };
    xmp.tag(metas, parsed);
#endif
    const GLsizei width = static_cast<GLsizei>(metas.width);
    const GLsizei height = static_cast<GLsizei>(metas.height);

    std::vector<std::shared_ptr<uint8_t>> buffers { pool.memory() };
    functions->glBindFramebuffer(GL_READ_FRAMEBUFFER, quad.fbo);
    functions->glReadBuffer(GL_COLOR_ATTACHMENT0);
    functions->glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[0].get());
    functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
#ifdef XMP
    std::thread(compressGreyImage, config->jpegQuality(), metas.width, metas.height, xmp, filename, buffers).detach();
#else
    std::thread(compressGreyImage, config->jpegQuality(), metas.width, metas.height, filename, buffers).detach();
#endif
}

void JpegRecorderPrivate::renderAsYuv(const GLQuadRendering &quad, const PboMetas &metas)
{
    // on rejette les images sans date
    if (metas.timestamp == 0) {
        qDebug("Jpeg recorder failed to save 0 timestamp image");
        return;
    }


    const ParsedTimecode parsed = parseTimecode(metas.timestamp);
    const QString filename = config->jpegEffectiveRecordingDir().filePath(toFilename(parsed));

    // on rejette les images en double
    if (QFile::exists(filename)) {
        qDebug("Jpeg recorder failed to save, image exist");
        return;
    }

#ifdef XMP
    VtoXmp xmp { config->jpegXmp() };
    xmp.tag(metas, parsed);
#endif
    std::vector<std::shared_ptr<uint8_t>> buffers { pool.memory(), uvPool.memory(), uvPool.memory() };

    functions->glBindFramebuffer(GL_READ_FRAMEBUFFER, quad.fbo);
    // on bind la texture du fbo principal
    functions->glBindTexture(GL_TEXTURE_RECTANGLE, quad.outTexture);

    // le rendu dans notre fbo
    fboPtr->bind();
    // premier rendu full size pour l'extraction de Y

    programPtr->bind();
    vao.bind();
    functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    vao.release();
    programPtr->release();
    GLsizei width = static_cast<GLsizei>(metas.width);
    GLsizei height = static_cast<GLsizei>(metas.height);
    functions->glReadBuffer(GL_COLOR_ATTACHMENT0);
    functions->glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[0].get());

    // deuxième rendu 1/2 size pour U et V
    scaledProgramPtr->bind();
    vao.bind();
    functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    vao.release();
    scaledProgramPtr->release();
    // le shader scale à 1/2 et place la texture en haut à gauche
    width /= 2;
    height /= 2;
    functions->glReadBuffer(GL_COLOR_ATTACHMENT1);
    functions->glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[1].get());
    functions->glReadBuffer(GL_COLOR_ATTACHMENT2);
    functions->glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[2].get());

    // restaure le fbo
    functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
#ifdef XMP
    std::thread(compressYuvImage, config->jpegQuality(), metas.width, metas.height, xmp, filename, buffers).detach();
#else
    std::thread(compressYuvImage, config->jpegQuality(), metas.width, metas.height, filename, buffers).detach();
#endif
}

JpegRecorderPrivate::JpegRecorderPrivate(JpegRecorderFormat format)
    : format(format)
{
    render = (format == JpegRecorderFormat::Grey)?&JpegRecorderPrivate::renderAsGrey:&JpegRecorderPrivate::renderAsYuv;
}

JpegRecorderPrivate::~JpegRecorderPrivate() = default;

JpegRecorder::JpegRecorder(JpegRecorderFormat format)
    : GlEffect(*new JpegRecorderPrivate(format))
{}

JpegRecorder::~JpegRecorder() = default;

void JpegRecorder::initialize(QOpenGLContext *context, const GLQuadBuffers &quad)
{
    IMPL(JpegRecorder);
    GlEffect::initialize(context, quad);

    if (d->format == JpegRecorderFormat::Yuv) {
        d->programPtr.reset( new QOpenGLShaderProgram );
        d->programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
        d->programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/rgb2i420.frag");
        d->programPtr->link();

        d->scaledProgramPtr.reset( new QOpenGLShaderProgram );
        d->scaledProgramPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/scaleddefault.vert");
        d->scaledProgramPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/rgb2i420.frag");
        d->scaledProgramPtr->link();

        QMatrix4x4 mat;
        mat.setToIdentity();
        mat.translate(-0.5f, -0.5f);
        mat.scale(0.5f, 0.5f);
        d->scaledProgramPtr->bind();
        d->scaledProgramPtr->setUniformValue("transform", mat);
        d->scaledProgramPtr->release();

        d->vao.create();
        d->vao.bind();
        d->functions->glBindBuffer(GL_ARRAY_BUFFER, quad.vbo);
        d->functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0));
        d->functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(2 * sizeof (GLfloat)));
        d->functions->glEnableVertexAttribArray(0);
        d->functions->glEnableVertexAttribArray(1);
        d->functions->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quad.iVbo);
        d->vao.release();

    }
}

void JpegRecorder::changeGeometry(const QSize &size)
{
    IMPL(JpegRecorder);

    d->pool.configure(static_cast<std::size_t>(size.width() * size.height()));
    QOpenGLFramebufferObjectFormat fboFormat;
    // on utilise le rendu dans un rectangle
    fboFormat.setTextureTarget(QOpenGLTexture::TargetRectangle);
    fboFormat.setInternalTextureFormat(QOpenGLTexture::RGB8_UNorm);
    d->fboPtr.reset( new QOpenGLFramebufferObject(size, fboFormat) );

    if (d->format ==JpegRecorderFormat::Yuv) {
        d->uvPool.configure(static_cast<std::size_t>(size.width() * size.height() / 4));

        // attache 2 autre textures pour U et V
        d->fboPtr->addColorAttachment(size);
        d->fboPtr->addColorAttachment(size);
        GLenum bufs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
        d->functions->glDrawBuffers(3, bufs);
    }
}

void JpegRecorder::destroy()
{
    IMPL(JpegRecorder);

    if (d->programPtr) {
        d->programPtr->removeAllShaders();
    }
    if (d->scaledProgramPtr) {
        d->scaledProgramPtr->removeAllShaders();
    }
    if (d->vao.isCreated()) {
        d->vao.destroy();
    }
}

void JpegRecorder::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    IMPL(JpegRecorder);

    if (d->config->jpegIsRecording()) {
        (d->* (d->render))(fbo, metas);
    }
}



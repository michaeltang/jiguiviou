/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ptpderivedtypes.h"

#include <QVector>
#include <QString>
#include <QtEndian>

using namespace PTP::DerivedTypes;

Timestamp::Timestamp(quint8 * const memory)
    : timestamp(memory)
{}

quint64 Timestamp::secondsField() const
{
    return Q_UINT64_C(0)
            | (static_cast<quint64>(timestamp[0]) << 32)
            | (static_cast<quint64>(timestamp[1]) << 24)
            | (static_cast<quint64>(timestamp[2]) << 16)
            | (static_cast<quint64>(timestamp[3]) << 8)
            | (static_cast<quint64>(timestamp[4]));
}

quint32 Timestamp::nanosecondsField() const
{
    return qFromBigEndian<quint32>(timestamp + 6);
}

int Timestamp::size()
{
    return 10;
}

ClockIdentity::ClockIdentity(quint8 * const memory)
    : clockIdentity(memory)
{}

QVector<quint8> ClockIdentity::identity() const
{
    QVector<quint8> ret(size());
    ::memcpy(ret.data(), clockIdentity, size());
    return ret;
}

void ClockIdentity::setIdentity(const QVector<quint8> &value)
{
    Q_ASSERT(value.size() == 8);
    ::memcpy(clockIdentity, value.constData(), value.size());
}

int ClockIdentity::size()
{
    return 8;
}

PortIdentity::PortIdentity(quint8 * const memory)
    : portIdentity(memory)
{}

ClockIdentity PortIdentity::clockIdentity()
{
    return ClockIdentity(portIdentity);
}

quint16 PortIdentity::portNumber() const
{
    return qFromBigEndian<quint16>(portIdentity + 8);
}

void PortIdentity::setPortNumber(quint16 port)
{
    qToBigEndian<quint16>(port, portIdentity + 8);
}

int PortIdentity::size()
{
    return 10;
}

PortAddress::PortAddress()
    : memory(4, 0),
      me(memory.data())
{
    Q_ASSERT(me != 0);
}

PortAddress::PortAddress(quint8 *data)
    : me(data)
{}

PortAddress::PortAddress(NetworkProtocol::Value networkProtocol, const QVector<quint8> &address)
    : memory(4 + address.size(), 0),
      me(memory.data())
{
    Q_ASSERT(me != 0);
    Q_ASSERT(address.size() <= 0xFFFF);
    qToBigEndian<qint16>(static_cast<qint16>(networkProtocol), me);
    qToBigEndian<quint16>(static_cast<quint16>(address.size()), me + 2);
    ::memcpy(me + 4, address.constData(), address.size());
}

PTP::NetworkProtocol::Value PortAddress::networkProtocol() const
{
    return static_cast<PTP::NetworkProtocol::Value>(qFromBigEndian<qint16>(me));
}

quint16 PortAddress::addressLength() const
{
    return qFromBigEndian<quint16>(me + 2);
}

const quint8 *PortAddress::addressField() const
{
    return static_cast<const quint8 *>(me + 4);
}

int PortAddress::size() const
{
    return 4 + addressLength();
}

ClockQuality::ClockQuality(quint8 * const memory)
    : clockQuality(memory)
{}

quint8 ClockQuality::clockClass() const
{
    return clockQuality[0];
}

quint8 ClockQuality::clockAccuracy() const
{
    return clockQuality[1];
}

quint16 ClockQuality::offsetScaledLogVariance() const
{
    return qFromBigEndian<quint16>(clockQuality + 2);
}

int ClockQuality::size()
{
    return 4;
}


Tlv::Tlv(quint8 *data)
    : me(data)
{}

Tlv::Tlv(int memorySize)
    : memory(memorySize, 0),
      me(memory.data())
{}

PTP::TlvType::Value Tlv::type() const
{
    return static_cast<TlvType::Value>(qFromBigEndian<qint16>(me));
}

quint16 Tlv::lengthField() const
{
    return qFromBigEndian<quint16>(me + 2);
}

quint8 *Tlv::data() const
{
    return me;
}

int Tlv::size() const
{
    return memory.size();
}

PtpText::PtpText()
    : memory(1, 0),
      me(memory.data())
{
    Q_ASSERT(me != 0);
}

PtpText::PtpText(quint8 *data)
    : me(data)
{}

PtpText::PtpText(const QString &text)
    : memory(1 + text.size(), 0),
      me(memory.data())
{
    Q_ASSERT(me != 0);
    Q_ASSERT(text.size() <= 127);

    me[0] = static_cast<qint8>(text.size());
    ::memcpy(me + 1, text.toUtf8().constData(), text.size());
}

int PtpText::lengthField() const
{
    return me[0];
}

QString PtpText::textField() const
{
    return QString(QString::fromUtf8(reinterpret_cast<const char *>(me + 1), me[0]));
}

const quint8 *PtpText::data() const
{
    return me;
}

int PtpText::size() const
{
    return 1 + lengthField();
}




















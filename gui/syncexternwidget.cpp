/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "syncexternwidget.h"
#include <externsync.h>

using namespace Jiguiviou;

#include <QObject>
#include <QRadioButton>
#include <QLineEdit>
#include <QFormLayout>

class Observer final : public QObject, public ExternSync::Observer
{
    Q_OBJECT

public:
    Observer(QObject *parent = nullptr) : QObject(parent)
    {}

    virtual void changeSyncMode(ExternSync::Mode newMode) override final
    { emit modeChanged(newMode); }

signals:
    void modeChanged(ExternSync::Mode newMode);

};
#include "syncexternwidget.moc"

SyncExternWidget::SyncExternWidget(QWidget *parent)
    : QWidget(parent)
{
    std::shared_ptr<int> mesagesCount { new int {0} };

    std::shared_ptr<Observer> observer { new Observer };
    QSharedPointer<ExternSync::Socket> externSync { new ExternSync::Socket };
    externSync->addObserver(observer);
    externSync->listen();

    QRadioButton *M50Hz = new QRadioButton(trUtf8("50 Hz"));
    connect(M50Hz, &QRadioButton::clicked, [externSync](bool checked) {
        if (checked) {
            externSync->setMode(ExternSync::Mode::M50Hz);
        }
    });

    QRadioButton *M25HzEven = new QRadioButton(trUtf8("25 Hz trames paires"));
    connect(M25HzEven, &QRadioButton::clicked, [externSync](bool checked) {
        if (checked) {
            externSync->setMode(ExternSync::Mode::M25Hz_EVEN);
        }
    });

    QRadioButton *M25HzOdd = new QRadioButton(trUtf8("25 Hz trames impaires"));
    connect(M25HzOdd, &QRadioButton::clicked, [externSync](bool checked) {
        if (checked) {
            externSync->setMode(ExternSync::Mode::M25Hz_ODD);
        }
    });

    QLineEdit *lastMessage = new QLineEdit;
    lastMessage->setReadOnly(true);
    lastMessage->setAlignment(Qt::AlignLeft);

    QFormLayout *layout = new QFormLayout;
    layout->addRow(trUtf8("Modes de synchronisation"), M50Hz);
    layout->addWidget(M25HzEven);
    layout->addWidget(M25HzOdd);
    layout->addRow(trUtf8("Dernier message reçu"), lastMessage);

    setLayout(layout);

    connect(observer.get(), &Observer::modeChanged, [M50Hz, M25HzEven, M25HzOdd, lastMessage, mesagesCount](ExternSync::Mode newMode) {
        *(mesagesCount.get()) += 1;
        switch (newMode) {
        case ExternSync::Mode::M50Hz:
            M50Hz->setChecked(true);
            lastMessage->setText(trUtf8("%0 \t Mode 50 Hz").arg(*mesagesCount.get(), 4, 10, QLatin1Char(' ')));
            break;
        case ExternSync::Mode::M25Hz_EVEN:
            M25HzEven->setChecked(true);
            lastMessage->setText(trUtf8("%0 \t Mode 25 Hz trames paires").arg(*mesagesCount.get(), 4, 10, QLatin1Char(' ')));
            break;
        case ExternSync::Mode::M25Hz_ODD:
            M25HzOdd->setChecked(true);
            lastMessage->setText(trUtf8("%0 \t Mode 25 Hz trames impaires").arg(*mesagesCount.get(), 4, 10, QLatin1Char(' ')));
            break;
        default:
            lastMessage->setText(trUtf8("%0 \t Message inconnu").arg(*mesagesCount.get(), 4, 10, QLatin1Char(' ')));
            break;
        }
    });

    externSync->setMode(ExternSync::Mode::CURRENT);
}



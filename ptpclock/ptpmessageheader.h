/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PTPMESSAGEHEADER_H
#define PTPMESSAGEHEADER_H

#include "ptpderivedtypes.h"
#include <QVector>

namespace PTP {

namespace MessageType {
enum Value {
    Sync = 0x0,
    Delay_Req = 0x1,
    Pdelay_Req = 0x2,
    Pdelay_Resp = 0x3,
    Follow_Up = 0x8,
    Delay_Resp = 0x9,
    Pdelay_Resp_Follow_Up = 0xA,
    Annouce = 0xB,
    Signaling = 0xC,
    Management = 0xD
};
}

namespace Flag {
enum Value {
    alternateMaster = 0x0001,
    twoStep = 0x0002,
    unicast = 0x0004,
    PTPprofileSpecific1 = 0x0020,
    PTPprofileSpecific2 = 0x0040,
    leap61 = 0x0100,
    leap59 = 0x0200,
    currentUtcOffsetvalid = 0x0400,
    ptpTimescale = 0x0800,
    timeTraceable = 0x1000,
    frequencyTraceable = 0x2000
};
}

namespace ControlField {
enum Value {
    Sync = 0x00,
    Delay_Req = 0x01,
    Follow_Up = 0x02,
    Delay_Resp = 0x03,
    Management = 0x04,
    AllOthers = 0x05
};
}

class MessageHeader
{
    QVector<quint8> m_selfMemory;

public:
    MessageHeader(char *data);
    MessageHeader(int memorySize);

    MessageType::Value messageType() const;
    void setMessageType(MessageType::Value type);

    int versionPTP() const;
    void setVersionPTP(quint8 version);

    quint16 messageLength() const;
    void setMessageLength(quint16 length);

    quint8 domainNumber() const;
    void setDomainNumber(quint8 number);

    bool flagField(Flag::Value flag) const;
    void setFlagField(Flag::Value flag, bool value = true);

    qint64 correctionField() const;
    void setCorrectionField(qint64 value);

    DerivedTypes::PortIdentity sourcePortIdentity();

    quint16 sequenceId() const;
    void setSequenceId(quint16 id);

    ControlField::Value controlField() const;
    void setControlField(ControlField::Value value);

    qint8 logMessageInterval() const;
    void setLogMessageInterval(qint8 interval);

    const quint8 *data() const;

    QString dump() const;

protected:
    static int size();
    quint8 *const m_me;

};




} // Ptp


#endif // PTPMESSAGEHEADER_H

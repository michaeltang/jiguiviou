/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "jpegrecorder.h"
#include "gvspqtreceiver.h"
#include "gvspqtreceiverconfig.h"


#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QSpinBox>
#include <QCheckBox>
#include <QFormLayout>

using namespace Jiguiviou;

JpegRecorder::JpegRecorder(QWidget *parent)
    : QWidget(parent)
{}

void JpegRecorder::visualizeReceiver(QSharedPointer<Jgv::Gvsp::Receiver> receiverPtr)
{
    auto qtReceiverPtr = receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>();
    if (!qtReceiverPtr) {
        qWarning("Timecode widget failed to cast receiver");
        return;
    }

    auto getConfigPtr = [qtReceiverPtr]() {
        return qtReceiverPtr->configPtr().toStrongRef();
    };

    auto *path = new QLineEdit;
    path->setReadOnly(true);
    auto config = getConfigPtr();
    if (config) {
        path->setText(config->jpegRecordingPath());
    }

    auto *change = new QPushButton(trUtf8("Changer"));

    auto *quality = new QSpinBox;
    quality->setRange(1, 100);
    if (config) {
        quality->setValue(static_cast<int>(config->jpegQuality()));
    }

    auto *autoIncrement = new QCheckBox;
    if (config) {
        autoIncrement->setChecked(config->jpegIsAutoIncremental());
    }

    connect(change, &QPushButton::clicked, [getConfigPtr, path] () {
        auto config = getConfigPtr();
        if (config) {
            QString dir = QFileDialog::getExistingDirectory(nullptr, trUtf8("Chemin des enregistrements"), config->jpegRecordingPath());
            if (!dir.isEmpty()) {
                path->setText(dir);
                config->setJpegRecordingPath(dir);
            }
        }
    });

    connect(quality, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [getConfigPtr](int value) {
        auto config = getConfigPtr();
        if (config) {
            config->setJpegQuality(static_cast<uint>(value));
        }
    });

    connect(autoIncrement, &QCheckBox::stateChanged, [getConfigPtr](bool changed) {
        auto config = getConfigPtr();
        if (config) {
            config->setJpegIsAutoIncremental(changed);
        }
    });

    auto *layout = new QFormLayout;
    layout->addRow(trUtf8("Chemin des enregistrements"), path);
    layout->addRow(trUtf8("Changer le chemin"), change);
    layout->addRow(trUtf8("Qualité Jpeg"), quality);
    layout->addRow(trUtf8("Créer un dossier à chaque enregistrement"), autoIncrement);
    setLayout(layout);
}

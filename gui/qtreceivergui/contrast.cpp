/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "contrast.h"
#include "gvspqtreceiver.h"
#include "gvspqtreceiverconfig.h"

#include <QCheckBox>
#include <QSpinBox>
#include <QSlider>
#include <QFormLayout>

using namespace Jiguiviou;

Contrast::Contrast(QWidget *parent)
    : QWidget(parent)
{}

void Contrast::visualizeReceiver(QSharedPointer<Jgv::Gvsp::Receiver> receiverPtr)
{
    auto qtReceiverPtr = receiverPtr.staticCast<Jgv::Gvsp::QtBackend::Receiver>();
    if (!qtReceiverPtr) {
        qWarning("Contrast widget failed to cast receiver");
        return;
    }

    auto getConfigPtr = [qtReceiverPtr]() {
        return qtReceiverPtr->configPtr().toStrongRef();
    };

    auto config = getConfigPtr();

    auto *visuHistoZone = new QCheckBox;
    if (config) {
        visuHistoZone->setChecked(config->nonHistogramZoneIsMasked());
    }

    auto *width = new QSpinBox;
    width->setSuffix("%");
    width->setRange(0, 100);
    if (config) {
        width->setValue(config->histogramSourceWidth());
    }

    auto *height = new QSpinBox;
    height->setSuffix("%");
    height->setRange(0, 100);
    if (config) {
        height->setValue(config->histogramSourceHeight());
    }

    connect(visuHistoZone, &QCheckBox::stateChanged, [getConfigPtr](bool changed) {
        auto config = getConfigPtr();
        if (config) {
            config->setNonHistogramZoneIsMasked(changed);
        }
    });

    connect(width, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [getConfigPtr](int value) {
        auto config = getConfigPtr();
        if (config) {
            config->setHistogramSourceWidth(value);
        }
    });

    connect(height, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [getConfigPtr](int value) {
        auto config = getConfigPtr();
        if (config) {
            config->setHistogramSourceHeight(value);
        }
    });

    auto *layout = new QFormLayout;
    layout->addRow(trUtf8("Afficher le masque de zone"), visuHistoZone);
    layout->addRow(trUtf8("Largeur de la zone"), width);
    layout->addRow(trUtf8("Hauteur de la zone"), height);

    setLayout(layout);
}

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef INODEVALUE_P_H
#define INODEVALUE_P_H

#include "inode_p.h"
#include "pvalue.h"

#include <QList>
#include <QMap>

namespace Jgv {

namespace GenICam {

namespace InodeValue {

namespace Properties {
constexpr const char * Value = "Value";
constexpr const char * Min = "Min";
constexpr const char * Max = "Max";
constexpr const char * Inc = "Inc";
constexpr const char * ValueIndexed = "ValueIndexed";
constexpr const char * pValue = "pValue";
constexpr const char * pMin = "pMin";
constexpr const char * pMax = "pMax";
constexpr const char * pInc = "pInc";
constexpr const char * pIndex = "pIndex";
constexpr const char * pValueIndexed = "pValueIndexed";
constexpr const char * Representation = "Representation";
constexpr const char * ValueDefault = "ValueDefault";
constexpr const char * pValueDefault = "pValueDefault";
constexpr const char * pValueCopy = "pValueCopy";
constexpr const char * Unit = "Unit";
}

namespace Attributes {
constexpr const char * Index = "Index";
}


template <typename T>
class ObjectPrivate : public Inode::ObjectPrivate
{
    using get = T (ObjectPrivate::*) () const;
    using set = void (ObjectPrivate::*) (T);

public:
    ObjectPrivate(QSharedPointer<IPort::Interface> iport)
        : Inode::ObjectPrivate(iport)
    {}

    pValue<T> pVal { 0 };
    pValue<T> pMin { std::numeric_limits<T>::lowest() };
    pValue<T> pMax { std::numeric_limits<T>::max() };
    pValue<T> pInc { 0 };
    pValue<T> pValueDefault { 0 };
    pValueInteger pIndex;


    QMap<qint64, T> indexes;
    QMap<qint64, pValue<T> > pIndexes;
    QList<pValue<T> > pValueCopies;

    QString representation;
    QString unit;

    get evalValue = &ObjectPrivate<T>::fakeValue;
    get evalMin = &ObjectPrivate<T>::fakeValue;
    get evalMax = &ObjectPrivate<T>::fakeValue;
    get evalInc = &ObjectPrivate<T>::fakeValue;

    set setValue = &ObjectPrivate::setFake;

    T fakeValue() const
    {
        qWarning("InodeValue %s: Fake Value", qPrintable(featureName));
        return -1;
    }

    T valueByPValue() const
    {
        return pVal.value();
    }

    T valueByIndex() const
    {
        const qint64 index = pIndex.value();
        if (indexes.contains(index)) {
            return indexes[index];
        }
        else if (pIndexes.contains(index)) {
            return pIndexes[index].value();
        }
        else {
            // on retourne la valeur par defaut
            return pValueDefault.value();
        }
    }

    T minByPMin() const
    {
        return pMin.value();
    }

    T minByPValue() const
    {
        return pVal.min();
    }

    T maxByPMax() const
    {
        return pMax.value();
    }

    T maxByPValue() const
    {
        return pVal.max();
    }

    T incByPInc() const
    {
        return pInc.value();
    }

    T incByPValue() const
    {
        return pVal.inc();
    }

    void setFake(T)
    {
        qWarning("InodeValue %s: set Fake Value", qPrintable(featureName));
    }


    void setByPValue(T value)
    {

        pVal.set(value);

        // met à jour les copies
        if (!pValueCopies.isEmpty()) {
            for (auto &copie: pValueCopies) {
                copie.set(value);
            }

//            typename QList< pValue<T> >::iterator it = pValueCopies.begin();
//            for (; it != pValueCopies.end(); ++it) {
//                (*it).set(value);
//            }
        }
    }

    void setByIndex(T value)
    {
        {
            auto it = qFind(indexes.constBegin(), indexes.constEnd(), value);
            if (it != indexes.constEnd()) {
                pIndex.set(it.key());
                return;
            }
        }
        {
            auto it = pIndexes.constBegin();
            for (; it!=pIndexes.constEnd(); ++it) {
                if (it.value().value() == value) {
                    pIndex.set(it.key());
                    return;
                }
            }
        }
        //qWarning("InodeValue: %s failed to set value by Index (not found) %lld", qPrintable(featureName), value);
    }

}; // class ObjectPrivate

} // namespace InodeValue

} // namespace GenICam

} // namespace Jgv



#endif // INODEVALUE_P_H

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtmemorypool.h"
#include "gvspqtmemorypool_p.h"


using namespace Jgv::Gvsp::QtBackend;

MemoryPool::MemoryPool()
    : p_impl(new MemoryPoolPrivate)
{}

MemoryPool::~MemoryPool()
{}

void MemoryPool::configure(std::size_t size)
{
    IMPL(MemoryPool);
    d->pool.clear();
    d->size = size;
    d->pool.emplace_back(new uint8_t[d->size], std::default_delete<uint8_t[]>());
}

std::shared_ptr<uint8_t> MemoryPool::memory()
{
    IMPL(MemoryPool);

    // un compteur à 1 signifie que seul le pool détient
    // une référence sur la mémoire
    for (auto const &mem: d->pool) {
        if (mem.use_count() == 1) {
            return mem;
        }
    }

    // tous les blocs mémoire sont en cours d'utilisation, on en ajoute 1
    d->pool.emplace_back(new uint8_t[d->size], std::default_delete<uint8_t[]>());
    return d->pool.back();
}

std::size_t MemoryPool::memorySize() const
{
    IMPL(const MemoryPool);
    return d->size;
}

std::size_t MemoryPool::pooolSize() const
{
    IMPL(const MemoryPool);
    return d->pool.size();
}



/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtpixeltransform.h"
#include "gvspqtpixeltransform_p.h"

#include "gvspqtreceiver.h"

#include "gvspqtgleffect.h"
#include "gvspqtfaketransform.h"
#include "gvspqtbayertransform.h"
#include "gvspqtrgbtransform.h"
#include "gvspqtgreytransform.h"
#include "gvspqtyuv12transform.h"
#include "gvspqtyuv16transform.h"
#include "gvspqtyuv24transform.h"
#include "gvsp.h"

#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLFramebufferObject>
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>

#include <QDebug>

using namespace Jgv::Gvsp::QtBackend;

void PixelTransformPrivate::loop()
{
    initialize();
    std::unique_lock<std::mutex> lock(renderMutex);
    while (running) {
        renderCondition.wait(lock);


        std::unique_lock<std::mutex> lock(queueMutex);
        while (!pboQueue.empty()) {
            //const auto &pbo = pboQueue.front();
            render(pboQueue.front());
            pboQueue.pop();
        }
    }
    destroy();
}

void PixelTransformPrivate::populateEffects()
{
    std::for_each(effects.begin(), effects.end(), [this](const auto &pair) {
        pair.second->setHistogramPtr(histograms);
    });
}

bool PixelTransformPrivate::initialize()
{
    // on a besoin d'un context sur le thread,
    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    context.reset(new QOpenGLContext);
    context->setFormat(fmt);

    if (sharedContext != nullptr) {
        context->setShareContext(sharedContext);
    }
    else {
        qWarning("Pixel transform: no shared context available !");
    }

    if (!context->create()) {
        qWarning("Image pipeline failed to create context ! Closing thread");
        return false;
    }

    //n_surface.reset(new QOffscreenSurface);
    surface.setFormat(context->format());
    surface.create();
    if (!context->makeCurrent(&surface)) {
        qWarning("PixelTransform failed to make context current");
        return false;
    }

    functions = context->versionFunctions<QOpenGLFunctions_4_2_Core>();
    if (functions == nullptr) {
        qWarning("PixelTransform failed to get OpenGL 4.2 core functions");
        return false;
    }

    sharedFboProgramPtr.reset( new QOpenGLShaderProgram );
    sharedFboProgramPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
    sharedFboProgramPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/default.frag");
    sharedFboProgramPtr->link();

    std::array<GLfloat, 16> coords {
        {-1.0f, -1.0f, 0.0f, 0.0f,
            1.0f, -1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, 0.0f, 1.0f}
    };
    vbo.create();
    vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    vbo.bind();
    vbo.allocate(coords.data(), coords.size() * sizeof(GLfloat));
    vbo.release();

    std::array<GLushort, 6> indices { {0, 1, 2, 0, 2, 3} };
    indicesVbo.create();
    indicesVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    indicesVbo.bind();
    indicesVbo.allocate(indices.data(), indices.size() * sizeof(GLushort));
    indicesVbo.release();

    vao.create();
    vao.bind();
    vbo.bind();
    indicesVbo.bind();
    functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0));
    functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(2 * sizeof (GLfloat)));
    functions->glEnableVertexAttribArray(0);
    functions->glEnableVertexAttribArray(1);
    vao.release();

    ShaderDatas shaderDatas { {400.0f, 400.0f} };
    functions->glGenBuffers(1, &gbo);
    functions->glBindBuffer(GL_UNIFORM_BUFFER, gbo);
    functions->glBufferData(GL_UNIFORM_BUFFER, sizeof(shaderDatas), &shaderDatas, GL_STATIC_DRAW);
    functions->glBindBuffer(GL_UNIFORM_BUFFER, 0);
    GLuint binding_point_index = 0;
    functions->glBindBufferBase(GL_UNIFORM_BUFFER, binding_point_index, gbo);


    std::for_each(effects.begin(), effects.end(), [this](const auto &pair) {
        pair.second->initialize(context.data(), { vbo.bufferId(), indicesVbo.bufferId() });
    });

    return true;
}

void PixelTransformPrivate::render(const PboMetas &pbo)
{
    Q_UNUSED(pbo)

    sharedFboProgramPtr->bind();
    fboPtr->bind();
    functions->glBindTexture(GL_TEXTURE_RECTANGLE, fboPtr->texture());
    sharedFboPtr->bind();
    //functions->glViewport(0, 0, pbo.width, pbo.height);
    //functions->glClearColor(0.0, 0.0, 0.0, 0.0);
    //functions->glClear(GL_COLOR_BUFFER_BIT);
    vao.bind();
    functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    vao.release();
    functions->glBindTexture(GL_TEXTURE_RECTANGLE, 0);
    sharedFboPtr->release();
    sharedFboProgramPtr->release();
    functions->glFinish();
}

void PixelTransformPrivate::changeGeometry(const QSize &size)
{

    ShaderDatas shaderDatas { {static_cast<GLfloat>(size.width()), static_cast<GLfloat>(size.height())} };
    functions->glBindBuffer(GL_UNIFORM_BUFFER, gbo);
    functions->glBufferData(GL_UNIFORM_BUFFER, sizeof(shaderDatas), &shaderDatas, GL_STATIC_DRAW);
    functions->glBindBuffer(GL_UNIFORM_BUFFER, 0);

    QOpenGLFramebufferObjectFormat fboFormat;
    // on utilise le rendu dans un rectangle
    fboFormat.setTextureTarget(QOpenGLTexture::TargetRectangle);
    fboFormat.setInternalTextureFormat(QOpenGLTexture::RGB8_UNorm);

    fboPtr.reset( new QOpenGLFramebufferObject(size, fboFormat) );
    sharedFboPtr.reset( new QOpenGLFramebufferObject(size, fboFormat) );
    functions->glViewport(0, 0, size.width(), size.height());

    std::for_each(effects.begin(), effects.end(), [&size](const auto &pair) {
        pair.second->changeGeometry(size);
    });

    auto video = renderer.toStrongRef();
    if (video) {
        video->setTextureToRender(sharedFboPtr->texture());
    }
}

void PixelTransformPrivate::destroy()
{
    std::for_each(effects.begin(), effects.end(), [](const auto &pair) {
        pair.second->destroy();
    });
    if (vbo.isCreated()) {
        vbo.destroy();
    }
    if (indicesVbo.isCreated()) {
        indicesVbo.destroy();
    }
    if (vao.isCreated()) {
        vao.destroy();
    }
    if (context) {
        context->doneCurrent();
    }
    surface.destroy();
}


PixelTransform::PixelTransform(PixelTransformPrivate &dd)
    : p_impl(&dd)
{
    IMPL(PixelTransform);
    d->populateEffects();
    d->renderThread = std::thread (&PixelTransformPrivate::loop, d);
}

PixelTransform::~PixelTransform()
{
    IMPL(PixelTransform);
    if (d->renderThread.joinable()) {
        d->running = false;
        std::unique_lock<std::mutex> lock(d->renderMutex);
        d->renderCondition.notify_all();
        d->renderMutex.unlock();
        d->renderThread.join();
    }
}

QSharedPointer<PixelTransform> PixelTransform::makeShared(quint32 pixelFormat)
{
    switch (pixelFormat) {
    case GVSP_PIX_MONO8:
        return QSharedPointer<PixelTransform>(new GreyTransform);
    case GVSP_PIX_BAYGR8:
    case GVSP_PIX_BAYRG8:
    case GVSP_PIX_BAYGB8:
    case GVSP_PIX_BAYBG8:
        return QSharedPointer<PixelTransform>(new BayerTransform);
    case GVSP_PIX_RGB8_PACKED:
    case GVSP_PIX_BGR8_PACKED:
        return QSharedPointer<PixelTransform>(new RgbTransform);
    case GVSP_PIX_YUV411_PACKED:
        return QSharedPointer<PixelTransform>(new Yuv12Transform);
    case GVSP_PIX_YUV422_PACKED:
        return QSharedPointer<PixelTransform>(new Yuv16Transform);
    case GVSP_PIX_YUV444_PACKED:
        return QSharedPointer<PixelTransform>(new Yuv24Transform);
    default:
        return QSharedPointer<PixelTransform>( new FakeTransform);
    }
}

bool PixelTransform::isHandledFormat(quint32 pixelFormat)
{
    switch (pixelFormat) {
    case GVSP_PIX_MONO8:
    case GVSP_PIX_BAYGR8:
    case GVSP_PIX_BAYRG8:
    case GVSP_PIX_BAYGB8:
    case GVSP_PIX_BAYBG8:
    case GVSP_PIX_RGB8_PACKED:
    case GVSP_PIX_BGR8_PACKED:
    case GVSP_PIX_YUV411_PACKED:
    case GVSP_PIX_YUV422_PACKED:
    case GVSP_PIX_YUV444_PACKED:
        return true;
    default:
        return false;
    }
}


bool PixelTransform::handlePixelFormat(quint32 pixelFormat) const
{
    IMPL(const PixelTransform);
    return d->pixelFormats.contains(pixelFormat);
}

void PixelTransform::setSharedContext(QOpenGLContext *context)
{
    IMPL(PixelTransform);
    d->sharedContext = context;
}

void PixelTransform::setGeneralConfig(ReceiverConfigPtr config)
{
    IMPL(PixelTransform);
    d->config = config;
    std::for_each(d->effects.begin(), d->effects.end(), [config](const auto &pair) {
        pair.second->setGeneralConfig(config);
    });
}

void PixelTransform::setVideoRender(QWeakPointer<GLVideoRenderer> renderer)
{
    IMPL(PixelTransform);

    d->renderer.swap(renderer);
    if (d->sharedFboPtr) {
        auto renderer = d->renderer.toStrongRef();
        if (renderer) {
            renderer->setTextureToRender(d->sharedFboPtr->texture());
        }
    }
}

void PixelTransform::renderPbo(const PboMetas &pbo)
{
    IMPL(PixelTransform);

    std::unique_lock<std::mutex> queueLock(d->queueMutex);
    d->pboQueue.emplace(std::move(pbo));

    std::unique_lock<std::mutex> renderLock(d->renderMutex);
    d->renderCondition.notify_all();
}

const PixelFormats &PixelTransform::pixelFormats() const
{
    IMPL(const PixelTransform);
    return d->pixelFormats;
}







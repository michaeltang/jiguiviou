
find_path(GENICAM_INCLUDE_DIR NAMES genicam.h)
mark_as_advanced(GENICAMT_INCLUDE_DIR)

find_library(GENICAM_LIBRARY NAMES genicam)
mark_as_advanced(GENICAM_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Genicam DEFAULT_MSG GENICAM_INCLUDE_DIR GENICAM_LIBRARY)

#set(GEVIPORT_LINK_FLAG)
#set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -lgvcpdevices")


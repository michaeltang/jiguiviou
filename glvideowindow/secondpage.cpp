/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "secondpage.h"
#include "secondpage_p.h"

#include "blacklevelpage.h"

using namespace Jgv::GLContainer;
#include <QDebug>
SecondPage::SecondPage()
    : Page( *new SecondPagePrivate)
{
    IMPL(SecondPage);



    auto button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Reticule);
    button->gridRect.moveTopLeft({3,1});
    auto notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setOverlayReticuleIsActive(!config->overlayReticuleIsActive());
        }
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::TimeCode);
    button->gridRect.moveTopLeft({4,1});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setOverlayTimecodeActive(!config->overlayTimecodeIsActive());
        }
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Histogram);
    button->gridRect.moveTopLeft({5,1});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setDrawHistogramIsActive(!config->drawHistogramIsActive());
        }
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::BlackLevel);
    button->gridRect.moveTopLeft({3,2});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    auto child = std::make_shared<BlackLevelPage>();
    d->childs.emplace_back(child);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [this, child]() {
        this->setVisible(false);
        child->setParent(this);
        child->setVisible(true);
    });
    d->widgets.emplace_back( std::move(button) );


    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Return);
    button->gridRect.moveTopLeft({6,3});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [this, d]() {
        this->setVisible(false);
        if (d->parent != nullptr) {
            d->parent->setVisible(true);
        }
    });
    d->widgets.emplace_back( std::move(button) );


}

SecondPage::~SecondPage() = default;











/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef ENDAT_H
#define ENDAT_H

#include "pimpl.hpp"
#include <memory>

enum class ConnectionStatus {
    Connected, Closed, Timeout,
    Reset, TramsmissionError, libeibError
};

struct EibNetwork {
    uint32_t ip;
    uint32_t netmask;
    uint32_t gateway;
    bool dhcp;
    uint32_t dhcpTimeout;
};

struct EibInfos {
    EibNetwork network;
    uint64_t mac;
    unsigned long numberOfOpenConnections;
    std::string id;
    std::string hostname;
};

/*!
 * \brief Structure AxeSettings
 *
 */
struct AxeSettings {
    /*!
     * \brief inverted
     *
     * Vrai si l'axe de rotation du codeur est inversé.
     */
    bool inverted;
    /*!
     * \brief origin
     *
     * Angle de départ de la rotation du codeur.
     *
     */
    int64_t origin;
    /*!
     * \brief dynamic
     *
     * Nombre de mesures d'angle sur un tour.
     */
    int64_t dynamic;
};

struct AxesReference {
    int64_t firstAxe;
    int64_t secondAxe;
};

struct EndatDatas {
    uint64_t timestamp;
    uint32_t triggerCount;
    int32_t firstPosition;
    int32_t secondPosition;
    uint32_t clientIP;
};

class EndatPrivate;
class Endat
{
public:
    Endat();
    ~Endat();

    void startSoftRealTime();

    uint32_t eibAddress() const;
    void setEibAddress(uint32_t address);

    void stop();

    EibInfos eibInfos() const;
    void setNetworkConfiguration(const EibNetwork &network);
    ConnectionStatus connectionStatus() const;

    void startPollingMode();

    AxeSettings axeSettings(int axeNumber) const;
    void setAxeSettings(int axeNumber, const AxeSettings &settings);

    AxesReference userAxesReference() const;
    AxesReference axesReference() const;
    void setUserAxesReference(const AxesReference &reference);
    void restoreAxesReference(const AxesReference &userReference, const AxesReference &reference);

    EndatDatas datas();

private:
    BASE_PIMPL(Endat)

};

#endif // ENDAT_H

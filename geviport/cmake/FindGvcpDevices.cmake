
find_path(GVCPDEVICES_INCLUDE_DIR NAMES gvcpclient.h)
mark_as_advanced(GVCPDEVICES_INCLUDE_DIR)

find_library(GVCPDEVICES_LIBRARY NAMES
    gvcpdevices
)
mark_as_advanced(GVCPDEVICES_LIBRARY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GvcpDevices DEFAULT_MSG GVCPDEVICES_INCLUDE_DIR GVCPDEVICES_LIBRARY)




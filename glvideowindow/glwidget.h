/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "pimpl.hpp"
#include <QRect>
#include <QOpenGLTexture>

class QPoint;
class QMatrix4x4;
class QImage;

namespace Jgv {

namespace GLWidget {

class WidgetPrivate;
class Widget
{
protected:
    Widget(WidgetPrivate &dd);
public:

    virtual ~Widget();

    virtual void setSizes(const QSize &widgetSize, const QSize &viewportSize);
    virtual QSize size() const;

    virtual void setPosition(const QPoint &position);
    virtual QPoint position() const;
    virtual QPoint bottomRight() const;

    virtual const QMatrix4x4 &transform() const;

    bool imageChanged() const;
    QImage image();

    std::unique_ptr<QOpenGLTexture> texture;
    QRect gridRect {0, 0, 1, 1};
    virtual bool event(QEvent *event);


private:
    BASE_PIMPL(Widget)

}; // class Widget

} // namespace GLWidget;

} // namespace Jgv

#endif // GLWIDGET_H

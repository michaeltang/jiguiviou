/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ptpclockdescription.h"

#include <QtEndian>

using namespace PTP::Management;

const int CLOCKDESCRIPTION_STATIC_SIZE = 14;

ClockDescription::ClockDescription()
    : ManagementTlv(nearEven(CLOCKDESCRIPTION_STATIC_SIZE + (4 * DerivedTypes::PtpText().size()) + DerivedTypes::PortAddress().size()))
{
    setManagementId(Id::ClockDescription);
}

ClockDescription::ClockDescription(quint8 *data)
    : ManagementTlv(data)
{}

ClockDescription::ClockDescription(const DerivedTypes::PtpText &physicalLayerprotocol, const QVector<quint8> &physicalAddress,
                                   const DerivedTypes::PortAddress &protocolAddress, const DerivedTypes::PtpText &productDescription,
                                   const DerivedTypes::PtpText &revisionData, const DerivedTypes::PtpText &userDescription)
    : ManagementTlv(nearEven(CLOCKDESCRIPTION_STATIC_SIZE + physicalLayerprotocol.size()
                    + physicalAddress.size() + productDescription.size() + protocolAddress.size()
                    + revisionData.size() + userDescription.size()))
{
    setManagementId(Id::ClockDescription);
}

bool ClockDescription::clockType(PTP::ClockType::Value type) const
{
    const quint16 ct = qFromBigEndian<quint16>(dataField());
    return (ct & static_cast<quint16>(type)) > 0;
}

PTP::DerivedTypes::PtpText ClockDescription::physicalLayerProtocol() const
{
    return DerivedTypes::PtpText(dataField() + 2);
}

quint16 ClockDescription::physicalAddressLength() const
{
    return qFromBigEndian<quint16>(dataField() + 2 + physicalLayerProtocol().size());
}

const quint8 *ClockDescription::physicalAddress() const
{
    return dataField() + 4 + physicalLayerProtocol().size();
}

PTP::DerivedTypes::PortAddress ClockDescription::protocolAddress() const
{
    return DerivedTypes::PortAddress(dataField() + 4 + physicalLayerProtocol().size() + physicalAddressLength());
}

const quint8 *ClockDescription::manufacturerIdentity() const
{
    return (dataField() + 4 + physicalLayerProtocol().size() + physicalAddressLength() + protocolAddress().size());
}

PTP::DerivedTypes::PtpText ClockDescription::productDescription() const
{
    return DerivedTypes::PtpText(dataField() + 4 + physicalLayerProtocol().size() + physicalAddressLength()
                                 + protocolAddress().size() + 4);
}

PTP::DerivedTypes::PtpText ClockDescription::revisiondata() const
{
    return DerivedTypes::PtpText(dataField() + 4 + physicalLayerProtocol().size() + physicalAddressLength()
                                 + protocolAddress().size() + 4 + productDescription().size());
}

PTP::DerivedTypes::PtpText ClockDescription::userDescription() const
{
    return DerivedTypes::PtpText(dataField() + 4 + physicalLayerProtocol().size() + physicalAddressLength()
                                 + protocolAddress().size() + 4 + productDescription().size() + revisiondata().size());
}

const quint8 *ClockDescription::profileIdentity() const
{
    return dataField() + 4 + physicalLayerProtocol().size() + physicalAddressLength()
            + protocolAddress().size() + 4 + productDescription().size() + revisiondata().size()
            + userDescription().size();
}


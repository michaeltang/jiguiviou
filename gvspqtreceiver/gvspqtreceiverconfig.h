/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTRECEIVERCONFIG_H
#define GVSPQTRECEIVERCONFIG_H

#include "gvspreceiver.h"

class QString;
class QDir;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

enum class TimecodeAlignment;

class VtoXmp;
class ReceiverConfigPrivate;
class ReceiverConfig final
{
public:
    ReceiverConfig();
    ~ReceiverConfig();

    void setJpegIsRecording(bool record);
    bool jpegIsRecording();
    void setJpegRecordingPath(const QString &path);
    QString jpegRecordingPath();
    QDir jpegEffectiveRecordingDir();
    void setJpegIsAutoIncremental(bool autoIncrement = true);
    bool jpegIsAutoIncremental();
    void setJpegQuality(uint quality);
    uint jpegQuality();
#ifdef XMP
    void setJpegXmpMetadata(const QString &key, const QString &value);
    QString jpegXmpMetadata(const QString &key);
    VtoXmp jpegXmp();
#endif

    void setOverlayTimecodeActive(bool active);
    bool overlayTimecodeIsActive();
    void setOverlayTimecodeFontHeight(int size);
    int overlayTimecodeFontSize();
    void setOverlayTimecodeAlignment(TimecodeAlignment alignment);
    TimecodeAlignment overlayTimecodeAlignment();

    void setOverlayReticuleIsActive(bool active);
    bool overlayReticuleIsActive();

    bool blackLevelNeedUpdate() const;
    void setBlackLevelUpdated();
    bool blackLevelIsActive() const;
    void setBlackLevelIsActive(bool active);
    void setBlackLevelNeedUpdate(bool) {}


    bool getHistogramIsActive();
    void setHistogramEqIsActive(bool active);
    bool histogramEqIsActive();
    void setDrawHistogramIsActive(bool active);
    bool drawHistogramIsActive();
    void setNonHistogramZoneIsMasked(bool masked);
    bool nonHistogramZoneIsMasked();
    void setHistogramSourceWidth(int width);
    int histogramSourceWidth();
    void setHistogramSourceHeight(int height);
    int histogramSourceHeight();

private:
    BASE_PIMPL(ReceiverConfig)
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv


#endif // GVSPQTRECEIVERCONFIG_H

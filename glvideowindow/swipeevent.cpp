/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "swipeevent.h"
#include "swipeevent_p.h"

#include <QEvent>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QLineF>
#include <QDebug>

using namespace Jgv;

const qreal D_LENGHT = 0.3;
const qint64 DURATION = 800;
const int D_ANGLE = 15;

SwipeEvent::SwipeEvent()
    : p_impl(new SwipeEventPrivate)
{}

SwipeDirection SwipeEvent::direction(QEvent *event)
{
    IMPL(SwipeEvent);
    if (event->type() == QEvent::MouseButtonPress) {
        d->start = static_cast<QMouseEvent*>(event)->pos();
        d->timer.restart();
        return SwipeDirection::none;
    }

    if (event->type() == QEvent::MouseButtonRelease) {
        if (!d->size.isValid()) {
            return SwipeDirection::none;
        }
        const auto elapsed = d->timer.elapsed();
        if (elapsed>0 && elapsed<DURATION) {

            auto height = [](const QLineF &line, const QSizeF &size) {
                return line.length() / size.height();
            };
            auto width = [](const QLineF &line, const QSizeF &size) {
                return line.length() / size.width();
            };

            const QLineF segment { d->start, static_cast<QMouseEvent*>(event)->pos() };
            const auto angle = segment.angle();

            if ((angle<D_ANGLE || angle>360-D_ANGLE) && width(segment, d->size)>D_LENGHT ) {
                return SwipeDirection::right;
            }
            if ((angle-90<D_ANGLE || angle-90>360-D_ANGLE) && height(segment, d->size)>D_LENGHT ) {
                return SwipeDirection::top;
            }
            if ((angle-180<D_ANGLE || angle-180>360-D_ANGLE) && width(segment, d->size)>D_LENGHT ) {
                return SwipeDirection::left;
            }
            if ((angle-270<D_ANGLE || angle-270>360-D_ANGLE) && height(segment, d->size)>D_LENGHT ) {
                return SwipeDirection::bottom;
            }
            return SwipeDirection::none;
        }
    }

    if (event->type() == QEvent::Resize) {
        d->size = static_cast<QResizeEvent*>(event)->size();
    }

    return SwipeDirection::none;
}

SwipeEvent::~SwipeEvent() = default;

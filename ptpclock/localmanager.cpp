/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "localmanager.h"
#include "localmanager_p.h"


#include "ptpclockdescription.h"
#include "ptpmessages.h"

#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <cstring>

#include <QFile>
#include <QDebug>

const QString DEFAULT_UDS = "/tmp/prout";
const QString UDS_PATH = "/var/run/ptp4l";

LocalManager::LocalManager(QObject *parent)
    : QObject(parent),
      d(new LocalManagerPrivate)
{
    d->socketDescriptor = 0;
}

LocalManager::~LocalManager()
{
    if (d->socketDescriptor > 0) {
        close(d->socketDescriptor);
        unlink(DEFAULT_UDS.toLatin1().data());
    }
}



bool LocalManager::connectManager()
{
    // socket uds (unix domain socket)
    d->socketDescriptor = socket(AF_LOCAL, SOCK_DGRAM, 0);
    if (d->socketDescriptor < 0) {
        qDebug("uds: failed to create socket");
        return false;
    }

    // addresse du pipe
    sockaddr_un sa = {};
    sa.sun_family = AF_LOCAL;
    std::memcpy(sa.sun_path, DEFAULT_UDS.toLatin1().data(), DEFAULT_UDS.toLatin1().size() + 1);

    unlink(DEFAULT_UDS.toLatin1().data());

    int err = bind(d->socketDescriptor, reinterpret_cast<sockaddr *>(&sa), sizeof(sa));

    if (err < 0) {
        qDebug("uds: bind failed: %s", strerror(errno));
        return false;
    }

    d->notifier = new QSocketNotifier(d->socketDescriptor, QSocketNotifier::Read, this);
    connect(d->notifier, SIGNAL(activated(int)), this, SLOT(onReadyRead(int)));

    return true;
}

void LocalManager::send()
{

    PTP::Management::ManagementMessage message(PTP::Management::Action::GET, PTP::Management::ClockDescription());


    // addresse du pipe
    sockaddr_un sa = {};
    sa.sun_family = AF_LOCAL;
    std::memcpy(sa.sun_path, UDS_PATH.toLatin1().data(), UDS_PATH.toLatin1().size() + 1);

    int cnt = sendto(d->socketDescriptor, message.data(), message.messageLength() + 1, 0, reinterpret_cast<sockaddr *>(&sa), sizeof(sa));
    if (cnt <= 0 ) {
        qDebug("uds: sendto failed: %s",strerror(errno));
    }
    qDebug("%s", qPrintable(message.dump()));
}

void LocalManager::onReadyRead(int /*sd*/)
{
    qDebug("LocalManager::onReadyRead");
    QByteArray kk;
    kk.resize(300);

    // addresse du pipe
    sockaddr_un sa = {};
    sa.sun_family = AF_LOCAL;
    std::memcpy(sa.sun_path, UDS_PATH.toLatin1().data(), UDS_PATH.toLatin1().size() + 1);
    socklen_t l = sizeof(sa);

    int cnt = recvfrom(d->socketDescriptor, kk.data(), kk.size() + 1, 0, reinterpret_cast<sockaddr *>(&sa), &l);
    if (cnt <= 0) {
        qDebug() << "uds: sendto failed: %m" << strerror(errno);
    }
    qDebug() << "plop" << cnt;
    PTP::Management::ManagementMessage mess(kk.data());
    qDebug() << "message type" << mess.messageType();
    qDebug() << "message length" << mess.messageLength();
    qDebug() << "message action" << mess.actionField();
    qDebug() << "management ID" << mess.managementTlv().managementId();

    switch (mess.managementTlv().managementId()) {
    case PTP::Management::Id::ClockDescription:
    {
        qDebug() << "message clockDescription";
        PTP::Management::ClockDescription cd(mess.managementTlv().data());
        qDebug() << "phys lay prot" << cd.physicalLayerProtocol().textField();
        qDebug() << "phy address length" << cd.physicalAddressLength();
        QString address;
        for (int i=0; i<cd.protocolAddress().addressLength(); ++i) {
            address.append(QString::number(cd.protocolAddress().addressField()[i]));
            address.append(" ");
        }
        qDebug() << "prot address" << cd.protocolAddress().networkProtocol() << address;
        qDebug() << "manufacturer ID" << cd.manufacturerIdentity()[0] << cd.manufacturerIdentity()[1] << cd.manufacturerIdentity()[2];
        qDebug() << "product desc" << cd.productDescription().textField();
        qDebug() << "rev data" << cd.revisiondata().textField();
        qDebug() << "user desc" << cd.userDescription().textField();
        qDebug() << "profile ID" << cd.profileIdentity()[0] << cd.profileIdentity()[1] << cd.profileIdentity()[2]
                 << cd.profileIdentity()[3] << cd.profileIdentity()[4] << cd.profileIdentity()[5];
        break;
    }
    default:
        qDebug() << "unknow message";
    }
}



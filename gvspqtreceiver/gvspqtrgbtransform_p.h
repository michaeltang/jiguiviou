/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTRGBTRANSFORM_P_H
#define GVSPQTRGBTRANSFORM_P_H

#include "gvspqtpixeltransform_p.h"

#include <QScopedPointer>

class QOpenGLShaderProgram;
class QOpenGLTexture;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

class RgbTransformPrivate final : public PixelTransformPrivate
{
public:
    virtual ~RgbTransformPrivate() override final = default;

    QScopedPointer<QOpenGLShaderProgram> programPtr;
    QScopedPointer<QOpenGLTexture> inTexturePtr;

    virtual void populateEffects() override final;
    virtual bool initialize() override final;
    virtual void render(const PboMetas &pbo) override final;
    virtual void destroy() override final;

};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQTRGBTRANSFORM_P_H

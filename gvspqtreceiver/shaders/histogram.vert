#version 420 core

layout(location = 0) in vec4 position;
uniform mat4 transform;
uniform float max;

void main()
{
    vec4 flipScale = position * vec4(1.0, -1.0/max, 1.0, 1.0);
    gl_Position = transform * flipScale;
}

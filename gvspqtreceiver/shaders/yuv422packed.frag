#version 420 core

layout(binding = 0) uniform sampler2DRect tex;
layout(location = 0) out vec3 outColor;

in vec4 vTexCoord;
/*
   R = 1.1643(Y - 0.0625)                    + 1.5958(V - 0.5)
   G = 1.1643(Y - 0.0625) - 0.39173(U - 0.5) - 0.8129(V - 0.5)
   B = 1.1643(Y - 0.0625) + 2.01700(U - 0.5)
*/
const   mat3 YUVtoRGB = mat3( 1.16430,  1.16430,  1.16430,
                              0.00000, -0.39173,  2.01800,
                              1.59580, -0.81290,  0.00000 );
const   vec3 offset = vec3(0.0, -0.5, -0.5);

vec3 UY(ivec4 coord) {return vec3(texelFetch(tex, coord.xy).gr, texelFetch(tex, coord.zy).r);}
vec3 VY(ivec4 coord) {return vec3(texelFetch(tex, coord.xy).gr, texelFetch(tex, coord.wy).r).rbg;}

void main()
{
    ivec4 coord = ivec4(vTexCoord);
    vec3 yuv = ((coord.x%2)==0)?UY(coord):VY(coord);
    outColor = clamp(YUVtoRGB * (yuv + offset), 0.0, 1.0);
}


/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGET_P_H
#define GLWIDGET_P_H

#include <QSize>
#include <QRect>
#include <QMatrix4x4>
#include <QImage>

namespace Jgv {

namespace GLWidget {

class WidgetPrivate
{
public:
    WidgetPrivate() {matrix.setToIdentity();}
    virtual ~WidgetPrivate() = default;
    QSize viewportSize {10, 10};
    QRect rect {0,0,10,10};
    QMatrix4x4 matrix;
    QImage image { rect.size(), QImage::Format_ARGB32 };
    bool imageNeedUpdate = true;

    virtual void updateMatrix();
    virtual void updateImage();

}; // class WidgetPrivate

} // namespace GLWidget

} // namespace Jgv

#endif // GLWIDGET_P_H

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtopenglpipeline.h"
#include "gvspqtopenglpipeline_p.h"


#include "gvspqtpixeltransform.h"

#include <QOpenGLFunctions_4_2_Core>
#include <QOffscreenSurface>
#include <cstring>
#include <memory>
#include <iostream>

using namespace Jgv::Gvsp::QtBackend;

struct PoolMemoryBlockPrivate : public Jgv::Gvsp::MemoryBlockPrivate
{
    PoolMemoryBlockPrivate(std::shared_ptr<uint8_t> mem)
        : dataPtr(mem)
    {}
    std::shared_ptr<uint8_t> dataPtr;
};

class PoolMemoryBlock : public Jgv::Gvsp::MemoryBlock
{
public:
    PoolMemoryBlock(std::shared_ptr<uint8_t> mem);
    virtual ~PoolMemoryBlock();

};
PoolMemoryBlock::PoolMemoryBlock(std::shared_ptr<uint8_t> mem)
    : Jgv::Gvsp::MemoryBlock(mem.get())
{
    impl = std::make_shared<PoolMemoryBlockPrivate>(mem);
}
PoolMemoryBlock::~PoolMemoryBlock() = default;

void OpenGLPipelinePrivate::initialize()
{
    if (!context) {
        context.reset(new QOpenGLContext);
        QSurfaceFormat fmt;
        fmt.setMajorVersion(4);
        fmt.setMinorVersion(2);
        fmt.setProfile(QSurfaceFormat::CoreProfile);
        context->setFormat(fmt);
        if (sharedContext != nullptr) {
            context->setShareContext(sharedContext);
        }
        else {
            qWarning("OpenGL pipeline: no shared context available !");
        }
    }

    if (!context->create()) {
        qCritical("OpenGL pipeline failed to create upload context");
        return;
    }

    uploadSurfacePtr.reset(new QOffscreenSurface);
    uploadSurfacePtr->setFormat(context->format());
    uploadSurfacePtr->create();

}

void OpenGLPipelinePrivate::createPixelTransform(Jgv::Gvsp::Image &image)
{
    // le premier module, adapté au format du pixel
    auto pt = PixelTransform::makeShared(image.geometry.pixelFormat);
    pt->setGeneralConfig(config);
    pt->setVideoRender(renderer);
    pt->setSharedContext(sharedContext);
    // à partir d'ici, il est raisonnable de créer notre buffer de pixel sur la carte graphique
    context->makeCurrent(uploadSurfacePtr.data());
    for (auto &pbo: pboPtrs) {
        if (pbo->isCreated()) {
            pbo->destroy();
        }
        pbo.reset(new QOpenGLBuffer(QOpenGLBuffer::PixelUnpackBuffer));
        pbo->create();
        pbo->bind();
        pbo->allocate(static_cast<int>(image.dataSize));
        pbo->release();
    }
    transform.swap(pt);
}


OpenGLPipeline::OpenGLPipeline()
    : p_impl(new OpenGLPipelinePrivate)
{}

OpenGLPipeline::~OpenGLPipeline()
{
    IMPL(const OpenGLPipeline);

    for (auto &pbo: d->pboPtrs) {
        if (pbo->isCreated()) {
            pbo->destroy();
        }
    }

    if (d->uploadSurfacePtr) {
        d->uploadSurfacePtr->destroy();
    }

    if (d->context) {
        d->context->doneCurrent();
    }

}

Jgv::Gvsp::MemoryBlock OpenGLPipeline::allocate(std::size_t size) noexcept
{
    IMPL(OpenGLPipeline);
    if (d->pool.memorySize() != size) {
        d->pool.configure(size);
    }
    return PoolMemoryBlock(d->pool.memory());
}

void OpenGLPipeline::destroy(MemoryBlock &block) noexcept
{
    Q_UNUSED(block)
}

void OpenGLPipeline::push(Image &image) noexcept
{
    IMPL(OpenGLPipeline);

    // on se trouve sur le thread du receveur,
    // comme on va uploader les pixels sur celui-ci, on a besoin
    // d'un context valid
    if (!d->context) {
        d->initialize();
        if (!d->context) {
            return;;
        }
    }

    if (!d->context->isValid()) {
        qWarning("OpenGL pipeline: something wrong with context, not valid");
        d->initialize();
        return;
    }

    d->context->makeCurrent(d->uploadSurfacePtr.data());

    if (!d->transform) {
        d->createPixelTransform(image);
    }
    else {
        // teste la compatibilité du transform
        if (d->transform->handlePixelFormat(image.geometry.pixelFormat)) {
            auto functions  = d->context->versionFunctions<QOpenGLFunctions_4_2_Core>();
            d->pboIndex = (d->pboIndex + 1) % 1;
            d->pboPtrs[d->pboIndex]->bind();
            functions->glBufferData(GL_PIXEL_UNPACK_BUFFER, static_cast<int>(image.dataSize), nullptr, GL_STREAM_DRAW);
            uint8_t *p = static_cast<uint8_t *>(functions->glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY));
            if (p != nullptr) {
                std::memcpy(p, image.memory.data, image.dataSize);
                functions->glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
            }

            d->pboPtrs[d->pboIndex]->release();
#ifdef ENDAT
            d->transform->renderPbo({d->pboPtrs[d->pboIndex]->bufferId(),
                                     static_cast<int32_t>(image.geometry.width),
                                     static_cast<int32_t>(image.geometry.height),
                                     image.geometry.pixelFormat,
                                     image.timestamp,
                                     image.site,
                                     image.gisement});
#else
            d->transform->renderPbo({d->pboPtrs[d->pboIndex]->bufferId(),
                                     static_cast<int32_t>(image.geometry.width),
                                     static_cast<int32_t>(image.geometry.height),
                                     image.geometry.pixelFormat,
                                     image.timestamp});
#endif
        }
        else {
            d->createPixelTransform(image);
        }
    }
    d->context->doneCurrent();
}

QWeakPointer<ReceiverConfig> OpenGLPipeline::config() const
{
    IMPL(const OpenGLPipeline);
    return d->config;
}

void OpenGLPipeline::setSharedContext(QOpenGLContext *context)
{
    IMPL(OpenGLPipeline);
    d->sharedContext = context;
}

void OpenGLPipeline::setVideoRender(QWeakPointer<GLVideoRenderer> renderer)
{
    IMPL(OpenGLPipeline);
    d->renderer.swap(renderer);
    if (d->transform) {
        d->transform->setVideoRender(d->renderer);
    }
}






/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtxmp.h"

#include "gvspqtdefinitions.h"
#include "gvspqtpixeltransform.h"

#include <exiv2/exiv2.hpp>
#include <QString>


using namespace Jgv::Gvsp::QtBackend;
using namespace Exiv2;

namespace {
const std::string VTO_NAMESPACE {"vto"};
const std::string TRIEDRE {"Triedre"};
const std::string LYRE {"Lyre"};
const std::string CAMPAGNE {"Campagne"};
const std::string CRI {"CRI"};
const std::string FOCALE {"Focale"};
const std::string CAMERA {"Camera"};
const std::string TIMESTAMP {"Timestamp"};
const std::string DATE {"Date"};
const std::string UTC {"UTC"};
#ifdef ENDAT
const std::string UNITE_ANGLE {"UniteAngle"};
const std::string SITE {"Site"};
const std::string GISEMENT {"Gisement"};
#endif
}

VtoXmp::VtoXmp() : Exiv2::XmpData ()
{
    auto value = Exiv2::Value::create(Exiv2::xmpText);
    XmpProperties::registerNs("http://videotheodolite.fr/1.0/", VTO_NAMESPACE);
    value->read("JiguiViou");
    add(XmpKey("Xmp.dc.source"), value.get());
    value->read("empty");
    add(XmpKey(VTO_NAMESPACE, TRIEDRE), value.get());
    add(XmpKey(VTO_NAMESPACE, LYRE), value.get());
    add(XmpKey(VTO_NAMESPACE, CAMPAGNE), value.get());
    add(XmpKey(VTO_NAMESPACE, CRI), value.get());
    add(XmpKey(VTO_NAMESPACE, FOCALE), value.get());
    add(XmpKey(VTO_NAMESPACE, CAMERA), value.get());
    add(XmpKey(VTO_NAMESPACE, TIMESTAMP), value.get());
    add(XmpKey(VTO_NAMESPACE, DATE), value.get());
    add(XmpKey(VTO_NAMESPACE, UTC), value.get());
#ifdef ENDAT
    add(XmpKey(VTO_NAMESPACE, UNITE_ANGLE), value.get());
    add(XmpKey(VTO_NAMESPACE, SITE), value.get());
    add(XmpKey(VTO_NAMESPACE, GISEMENT), value.get());
#endif

}

void VtoXmp::setMeta(const QString &key, const QString &value)
{
    // teste si la clé existe dans le namespace
    auto it = findKey(XmpKey(VTO_NAMESPACE, key.toStdString()));
    if (it != end()) {
        it->setValue(value.toStdString());
    }
    else {
        qWarning("XMP, can't add value %s to key %s, key doesn't exist", qPrintable(key), qPrintable(value));
    }
}

QString VtoXmp::meta(const QString &key) const
{
    // teste si la clé existe dans le namespace
    const XmpKey xmpKey { VTO_NAMESPACE, key.toStdString() };

    auto it = findKey(xmpKey);
    if (it != end()) {
        return QString::fromStdString(it->value().toString());
    }
    return QString("XMP key not found %0").arg(QString::fromStdString(xmpKey.key()));
}

void VtoXmp::tag(const PboMetas &metas, const ParsedTimecode &parsed)
{
    setMeta(QString::fromStdString(TIMESTAMP), QString::number(metas.timestamp));
    setMeta(QString::fromStdString(DATE), QString("%0 %1 %2").arg(parsed.year, 4, 10, QLatin1Char('0'))
            .arg(parsed.month, 2, 10, QLatin1Char('0')).arg(parsed.day, 2, 10, QLatin1Char('0')));
    setMeta(QString::fromStdString(UTC), QString("%0:%1:%2.%3").arg(parsed.hour, 2, 10, QLatin1Char('0'))
            .arg(parsed.minute, 2, 10, QLatin1Char('0')).arg(parsed.second, 2, 10, QLatin1Char('0')).arg(parsed.milli, 3, 10, QLatin1Char('0')));
#ifdef ENDAT
    const double site = static_cast<double>(metas.site)/1000.0;
    setMeta(QString::fromStdString(SITE), QString("%0").arg(site, site<0?8:7, 'f', 3, QLatin1Char('0')));
    const double gisement = static_cast<double>(metas.gisement)/1000.0;
    setMeta(QString::fromStdString(GISEMENT), QString("%0").arg(gisement, gisement<0?8:7, 'f', 3, QLatin1Char('0')));
#endif

}

void VtoXmp::saveToFile(const uchar *mem, std::size_t size, const QString &filename, const VtoXmp &xmp)
{
    auto image = ImageFactory::open(mem, static_cast<long>(size));
    image->setXmpData(xmp);
    image->writeMetadata();

    std::string xmpPacket;
    if (0 != Exiv2::XmpParser::encode(xmpPacket, xmp)) {
        throw Exiv2::Error(1, "Failed to serialize XMP data");
    }

    FileIo file { filename.toStdString() };
    file.open();
    file.transfer(image->io());
    file.close();
}


/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTGLEFFECT_H
#define GVSPQTGLEFFECT_H

#include "pimpl.hpp"
#include "gvspdevices.h"
#include "gvspqtdefinitions.h"

class QSize;
class QOpenGLContext;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

struct PboMetas;
class GlEffectPrivate;
class GlEffect
{
protected:
    explicit GlEffect(GlEffectPrivate &dd);
public:
    virtual ~GlEffect();

    virtual void initialize(QOpenGLContext *context, const GLQuadBuffers &quad) = 0;
    virtual void changeGeometry(const QSize &size) = 0;
    virtual void destroy() = 0;
    virtual void setGeneralConfig(ReceiverConfigPtr config);
    virtual void setHistogramPtr(HistogramsPtr) {}
    virtual void render(const GLQuadRendering &fbo, const PboMetas &metas) = 0;

    BASE_PIMPL(GlEffect)
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv


#endif // GVSPQTGLEFFECT_H

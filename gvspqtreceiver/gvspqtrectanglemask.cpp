/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtrectanglemask.h"
#include "gvspqtrectanglemask_p.h"

#include "gvspqtpixeltransform.h"

#include <QOpenGLFunctions_4_2_Core>

using namespace Jgv::Gvsp::QtBackend;

RectangleMask::RectangleMask()
    : GlEffect(*new RectangleMaskPrivate)
{}

RectangleMask::~RectangleMask() = default;

void RectangleMask::initialize(QOpenGLContext *context, const GLQuadBuffers &quad)
{
    IMPL(RectangleMask);
    GlEffect::initialize(context, quad);

    d->programPtr.reset(new QOpenGLShaderProgram);
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
    d->programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/rectanglemask.frag");
    d->programPtr->link();

    d->vao.create();
    d->vao.bind();
    d->functions->glBindBuffer(GL_ARRAY_BUFFER, quad.vbo);
    d->functions->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(0));
    d->functions->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof (GLfloat), reinterpret_cast<const GLvoid *>(2 * sizeof (GLfloat)));
    d->functions->glEnableVertexAttribArray(0);
    d->functions->glEnableVertexAttribArray(1);
    d->functions->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quad.iVbo);
    d->vao.release();

}

void RectangleMask::changeGeometry(const QSize &/*size*/)
{
    //    IMPL(HistogramEqualization);



}

void RectangleMask::destroy()
{
    IMPL(RectangleMask);

    if (d->vao.isCreated()) {
        d->vao.destroy();
    }

    if (d->programPtr) {
        d->programPtr->removeAllShaders();
    }
}

void RectangleMask::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    Q_UNUSED(fbo)
    Q_UNUSED(metas)

    IMPL(RectangleMask);

    if (d->config->nonHistogramZoneIsMasked()) {
        d->programPtr->bind();

        if (d->maskWidth != d->config->histogramSourceWidth() || d->maskHeight != d->config->histogramSourceHeight()) {
            d->maskWidth = d->config->histogramSourceWidth();
            d->maskHeight = d->config->histogramSourceHeight();
            const auto maskWidth =  d->config->histogramSourceWidth() * metas.width / 100;
            const auto maskHeight =  d->config->histogramSourceHeight() * metas.height / 100;
            QVector4D coord;
            coord.setX((metas.width - maskWidth) / 2);
            coord.setY((metas.height - maskHeight) / 2);
            coord.setZ(coord.x() + maskWidth);
            coord.setW(coord.y() + maskHeight);
            d->programPtr->setUniformValue("rectanglePoints", coord);
        }

        d->functions->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo.fbo);
        d->functions->glEnable(GL_BLEND);
        d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        d->functions->glBlendEquation (GL_FUNC_ADD);
        d->vao.bind();
        d->functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
        d->vao.release();
        d->functions->glDisable(GL_BLEND);
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
        d->programPtr->release();

    }
}

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQOPENGLPIPELINE_P_H
#define GVSPQOPENGLPIPELINE_P_H

#include "gvspqtdefinitions.h"
#include "gvspqtreceiverconfig.h"
#include "gvspqtmemorypool.h"

#include <QVector>
#include <QSharedPointer>
#include <QOpenGLBuffer>

class QOffscreenSurface;
class QOpenGLContext;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

class PixelTransform;
class GLVideoRenderer;
using TransformPtr = QSharedPointer<PixelTransform>;
using PboPtr = QSharedPointer<QOpenGLBuffer>;

class OpenGLPipelinePrivate
{
public:
    QOpenGLContext *sharedContext = nullptr;
    TransformPtr transform;
    QScopedPointer<QOpenGLContext> context;
    QScopedPointer<QOffscreenSurface> uploadSurfacePtr;
    QVector<PboPtr> pboPtrs{1, PboPtr(new QOpenGLBuffer(QOpenGLBuffer::PixelUnpackBuffer)) };
    int pboIndex = 0;
    QSharedPointer<ReceiverConfig> config { new ReceiverConfig };
    QWeakPointer<GLVideoRenderer> renderer;
    MemoryPool pool;

    void initialize();
    void createPixelTransform(Jgv::Gvsp::Image &image);

};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQOPENGLPIPELINE_P_H

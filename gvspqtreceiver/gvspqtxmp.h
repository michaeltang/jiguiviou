/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTVTOXMP_H
#define GVSPQTVTOXMP_H

#include <exiv2/exiv2.hpp>

class QString;

namespace Jgv
{

namespace Gvsp
{

namespace QtBackend
{

struct PboMetas;
struct ParsedTimecode;

class VtoXmpPrivate;
class VtoXmp : private Exiv2::XmpData
{
public:
    VtoXmp();
    void setMeta(const QString &key, const QString &value);
    QString meta(const QString &key) const;
    void tag(const PboMetas &metas, const ParsedTimecode &parsed);
    static void saveToFile(const unsigned char *mem, std::size_t size, const QString &filename, const VtoXmp &xmp);

}; // class VtoXmp

} // namespace QtBackend

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQTVTOXMP_H

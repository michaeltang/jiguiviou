#version 420 core

layout(binding = 0) uniform sampler2DRect tex;
layout(binding = 1) uniform sampler2DRect mask;
layout(location = 0) out vec4 outColor;

in vec2 vTexCoord;

void main()
{
    ivec2 coord = ivec2(vTexCoord);
    outColor = clamp(texelFetch(tex, coord).rrra - vec4(texelFetch(mask, coord).rrr, 0.), 0., 1.);
}

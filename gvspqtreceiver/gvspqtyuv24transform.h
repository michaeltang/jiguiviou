/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTYUV24TRANSFORM_H
#define GVSPQTYUV24TRANSFORM_H

#include "gvspqtpixeltransform.h"

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

class Yuv24TransformPrivate;
class Yuv24Transform final : public PixelTransform
{
public:
    Yuv24Transform();
    virtual ~Yuv24Transform() override final;
private:
    PIMPL(Yuv24Transform)
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv


#endif // GVSPQTYUV24TRANSFORM_H

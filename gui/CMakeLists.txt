
cmake_minimum_required(VERSION 3.0.0 FATAL_ERROR)

project(jiguiviou)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -O2 -march=native")
set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)


set(CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    "${CMAKE_SOURCE_DIR}/cmake/")

find_package(Qt5Widgets REQUIRED)
find_package(Qt5Network REQUIRED)
find_package(GvcpDevices REQUIRED)
find_package(GvspDevices REQUIRED)
find_package(GevIPort REQUIRED)
find_package(Genicam REQUIRED)

option(QTRECEIVER "Build with Qt gvsp receiver" OFF)
if (QTRECEIVER)
    message(STATUS "Qt receiver support")
    add_definitions(-DQTRECEIVER)
    find_package(GvspQtReceiver REQUIRED)
    add_subdirectory(qtreceivergui)
    include_directories(qtreceivergui)
endif()

option(ENDAT "Build with endat coder support via eib741" OFF)
if (ENDAT)
    message(STATUS "EIB741 support")
    add_definitions(-DENDAT)

    find_package(Endat REQUIRED)

    list(APPEND GUI_HEADERS
        endatwidget.h
        eibdialog.h)

    list(APPEND GUI_SOURCES
        endatwidget.cpp
        eibdialog.cpp)
endif()

option(EXTERNSYNC "Build with external GiGE synchro" OFF)
if (EXTERNSYNC)
    message(STATUS "External SYNC support")
    add_definitions(-DEXTERNSYNC)

    find_package(ExternSync REQUIRED)

    list(APPEND GUI_HEADERS
        syncexternwidget.h)

    list(APPEND GUI_SOURCES
        syncexternwidget.cpp)
endif()

list(APPEND GUI_HEADERS
    qtpimpl.hpp
    mainwidget.h mainwidget_p.h
    genicamtreeview.h
    htmltable.h
    jgvwizardpage.h jgvwizardpage_p.h
    discoverypage.h discoverypage_p.h
    forceippage.h forceippage_p.h
    networkselectionpage.h networkselectionpage_p.h
    cameravalidationpage.h cameravalidationpage_p.h
    gvsppage.h gvsppage_p.h
    gvspstatusbar.h gvspstatusbar_p.h
    gvspwidget.h
    lineseparator.h
    camerasession.h camerasession_p.h
    sessionstoolbar.h sessionstoolbar_p.h
    gvcputils.h gvcputils_p.h)

list(APPEND GUI_SOURCES
    mainwidget.cpp
    genicamtreeview.cpp
    htmltable.cpp
    jgvwizardpage.cpp
    discoverypage.cpp
    forceippage.cpp
    networkselectionpage.cpp
    cameravalidationpage.cpp
    gvsppage.cpp gvsppage_p.h
    gvspstatusbar.cpp
    gvspwidget.cpp
    lineseparator.cpp
    camerasession.cpp
    sessionstoolbar.cpp
    gvcputils.cpp)

if (EXTERNSYNC)
    list(APPEND GUI_SOURCES
        syncexternwidget.cpp)
endif()

add_executable(
    ${PROJECT_NAME}
    main.cpp
    ${GUI_SOURCES}
    ressources.qrc)

target_link_libraries(
    ${PROJECT_NAME}
    ${GVCPDEVICES_LIBRARY}
    ${GVSPDEVICES_LIBRARY}
    ${GEVIPORT_LIBRARY}
    ${GENICAM_LIBRARY}
    Qt5::Widgets
    Qt5::Network)

target_sources(${PROJECT_NAME} PRIVATE ${GUI_HEADERS})

if(QTRECEIVER)
    target_link_libraries(${PROJECT_NAME} qtreceivergui)
    target_link_libraries(${PROJECT_NAME} ${GVSPQTRECEIVER_LIBRARY})
endif()

if(ENDAT)
    target_link_libraries(${PROJECT_NAME} ${ENDAT_LIBRARY})
endif()

if(EXTERNSYNC)
    target_link_libraries(${PROJECT_NAME} ${EXTERNSYNC_LIBRARY})
endif()

find_program(
    FILECAP_EXECUTABLE
    filecap)

if(FILECAP_EXECUTABLE)
    set(FILECAP_FOUND true)
else(FILECAP_EXECUTABLE)
    message(WARNING "failed to find filecap, ${PROJECT_NAME} will not have capacities net_raw !")
    set(FILECAP_FOUND false)
endif(FILECAP_EXECUTABLE)

install(
    TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})


install(
    CODE
    "if(${FILECAP_FOUND} STREQUAL \"true\")
    message(STATUS \"doing filecap ${CMAKE_INSTALL_PREFIX}/bin/${PROJECT_NAME} net_raw dac_override sys_nice\")
    execute_process (
        COMMAND ${FILECAP_EXECUTABLE} ${CMAKE_INSTALL_PREFIX}/bin/${PROJECT_NAME} net_raw dac_override sys_nice
        RESULT_VARIABLE FILECAP_RESULT)
    if(FILECAP_RESULT)
        message( WARNING \"filecap failed !\")
    endif()
else()
    message( WARNING \"filecap failed, can't find cap-ng !\")
endif()"
)


/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGE_P_H
#define GLPAGE_P_H

#include <memory>
#include <vector>
#include <QObject>
#include <QWeakPointer>
#include "glbutton.h"
#include <gvspqtreceiverconfig.h>
#include "glgrid.h"

namespace Jgv {

namespace GLWidget {
class Widget;
}

namespace GLContainer {

struct NotifyWrapper : public QObject, public GLWidget::Notify
{
    Q_OBJECT
    void clicked() override { emit onClicked(); }
signals:
    void onClicked();
};

class Page;
class PagePrivate
{
public:
    bool isVisible = false;
    std::vector<std::unique_ptr<GLWidget::Widget>> widgets;
    std::vector<std::shared_ptr<Page>> childs;
    QWeakPointer<Jgv::Gvsp::QtBackend::ReceiverConfig> config;
    GLContainer::Grid grid {6, 10};
    Page *parent = nullptr;

}; // class PagePrivate

} // namespace GLContainer

} // namespace Jgv



#endif // GLPAGE_P_H

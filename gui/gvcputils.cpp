/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvcputils.h"
#include "gvcputils_p.h"
#include "bootstrapregisters.h"

#include <QHostAddress>
#include <QByteArray>

using namespace Jiguiviou;

GvcpUtils::GvcpUtils(QObject *parent)
    : QObject(parent), p_impl(new GvcpUtilsPrivate)
{
    qRegisterMetaType<Jgv::Gvcp::DISCOVERY_ACK>();
    p_impl->wrapper = std::make_shared<Wrapper>();
    connect(p_impl->wrapper.get(), &Wrapper::newDeviceDiscovered, this, &GvcpUtils::newDeviceDiscovered);
    connect(p_impl->wrapper.get(), &Wrapper::forceIpFinished, this, &GvcpUtils::forceIpSucces);
    p_impl->discoverer.addListener(p_impl->wrapper);
}

GvcpUtils::~GvcpUtils()
{}

void GvcpUtils::setController(const QNetworkAddressEntry &interface)
{
    QT_IMPL(GvcpUtils);
    d->controller = interface;
}

QNetworkAddressEntry GvcpUtils::controller() const
{
    QT_IMPL(const GvcpUtils);
    return d->controller;
}

QHostAddress GvcpUtils::controllerAddress() const
{
    QT_IMPL(const GvcpUtils);
    return d->controller.ip();
}

void GvcpUtils::setTransmitterAck(const Jgv::Gvcp::DISCOVERY_ACK &ack)
{
    QT_IMPL(GvcpUtils);
    d->transmitter = ack;
}

Jgv::Gvcp::DISCOVERY_ACK GvcpUtils::transmitterAck() const
{
    QT_IMPL(const GvcpUtils);
    return d->transmitter;
}

QHostAddress GvcpUtils::transmitterAddress() const
{
    QT_IMPL(const GvcpUtils);
    return QHostAddress(Jgv::Gvcp::DiscoveryAckHelper::currentIP(d->transmitter));
}

QHostAddress GvcpUtils::receiverAddress() const
{
    QT_IMPL(const GvcpUtils);
    // par défaut le receveur est le controleur
    return d->controller.ip();
}

void GvcpUtils::setGvspIsMulticast(bool multicast)
{
    QT_IMPL(GvcpUtils);
    d->transmitterIsMulticast = multicast;
}

bool GvcpUtils::gvspIsMulticast() const
{
    QT_IMPL(const GvcpUtils);
    return d->transmitterIsMulticast;
}

void GvcpUtils::setGvspMulticastGroup(const QHostAddress &address)
{
    QT_IMPL(GvcpUtils);
    d->multicastGroup = address;
}

QHostAddress GvcpUtils::gvspMulticastGroup() const
{
    QT_IMPL(const GvcpUtils);
    return d->multicastGroup;
}

void GvcpUtils::listenForBroadcast()
{
    QT_IMPL(GvcpUtils);
    d->discoverer.listen(d->controller.ip().toIPv4Address());
}

void GvcpUtils::stopBroadcastListener()
{
    QT_IMPL(GvcpUtils);
    d->discoverer.stop();
}

void GvcpUtils::discover(const QHostAddress &peerIP)
{
    QT_IMPL(GvcpUtils);
    d->discoverer.discover(peerIP.toIPv4Address());
}

void GvcpUtils::forceIP(quint64 mac, const QHostAddress &newIP, const QHostAddress &newNetmask, const QHostAddress &newGateway)
{
    QT_IMPL(GvcpUtils);
    d->discoverer.forceIP(mac, newIP.toIPv4Address(), newNetmask.toIPv4Address(), newGateway.toIPv4Address());
}

bool GvcpUtils::monitorTransmitter()
{
    QT_IMPL(GvcpUtils);
    return d->gvcp.monitorDevice(d->controller.ip().toIPv4Address(), Jgv::Gvcp::DiscoveryAckHelper::currentIP(d->transmitter));
}

void GvcpUtils::releaseTransmitter()
{
    QT_IMPL(GvcpUtils);
    d->gvcp.releaseDevice();
}

Jgv::Gvcp::CCPPrivilege GvcpUtils::transmitterAccessMode()
{
    QT_IMPL(GvcpUtils);
    std::vector<uint32_t> regs = d->gvcp.readRegisters({Jgv::Gvcp::enumType(Jgv::Gvcp::BootstrapAddress::ControlChannelPrivilege)});
    if (regs.size() > 0) {
        return Jgv::Gvcp::CCP::privilegeFromCCP(regs.at(0));
    }
    return Jgv::Gvcp::CCPPrivilege::AccesDenied;
}

QString GvcpUtils::readXmlFilenameFromDevice()
{
    QT_IMPL(GvcpUtils);

    return QString::fromStdString(d->gvcp.xmlFilename());
}

void GvcpUtils::setXmlFilename(const QString &name)
{
    QT_IMPL(GvcpUtils);
    d->xmlFilename = name;
}

QString GvcpUtils::xmlFilename() const
{
    QT_IMPL(const GvcpUtils);
    return d->xmlFilename;
}

QString GvcpUtils::readXmlFileFromDevice()
{
    QT_IMPL(GvcpUtils);

    return QString::fromStdString(d->gvcp.xmlFile());
}

void GvcpUtils::setXmlFile(const QString &file)
{
    QT_IMPL(GvcpUtils);
    d->xmlFile = file;
}

QString GvcpUtils::xmlFile() const
{
    QT_IMPL(const GvcpUtils);
    return d->xmlFile;
}

void GvcpUtils::setWantControl(bool wanted)
{
    QT_IMPL(GvcpUtils);
    d->wantcontrol = wanted;
}

bool GvcpUtils::wantControl() const
{
    QT_IMPL(const GvcpUtils);
    return d->wantcontrol;
}


/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "eibdialog.h"

#include <endat.h>
#include "htmltable.h"
#include "lineseparator.h"

#include <QFormLayout>
#include <QLabel>
#include <QHostAddress>
#include <QLineEdit>
#include <QCheckBox>
#include <QSpinBox>
#include <QPushButton>
#include <QTimer>
#include <QMessageBox>
#include <QSettings>
#include <QDialogButtonBox>

using namespace Jiguiviou;

namespace {

QString statusToString(ConnectionStatus status) {
    switch (status) {
    case ConnectionStatus::Closed: return "Non connecté";
    case ConnectionStatus::Connected: return "Connecté";
    case ConnectionStatus::Reset: return "Reset par Eib";
    case ConnectionStatus::Timeout: return "Timeout";
    case ConnectionStatus::TramsmissionError: return "Erreur transmission";
    case ConnectionStatus::libeibError: return "Erreur libeib";
    }
    return "Unknow";
}

QString macToString  (uint64_t address) {
    return QString("%0:%1:%2:%3:%4:%5")
            .arg(0xFF & (address >> 0x28), 2, 16, QLatin1Char('0'))
            .arg(0xFF & (address >> 0x20), 2, 16, QLatin1Char('0'))
            .arg(0xFF & (address >> 0x18), 2, 16, QLatin1Char('0'))
            .arg(0xFF & (address >> 0x10), 2, 16, QLatin1Char('0'))
            .arg(0xFF & (address >> 0x8), 2, 16, QLatin1Char('0'))
            .arg(0xFF & address, 2, 16, QLatin1Char('0'))
            .toUpper();
}

} // anonymous namespace

EibDialog::EibDialog(QWidget *parent)
    : QDialog(parent), m_endatPtr(new Endat)
{
    QSettings settings;
    settings.beginGroup("Eib741");
    QHostAddress ip {settings.value("Address", QHostAddress(m_endatPtr->eibAddress()).toString()).toString()};
    settings.endGroup();

    m_endatPtr->setEibAddress(ip.toIPv4Address());
    m_endatPtr->startPollingMode();

    QFormLayout *layout = new QFormLayout;
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);

    QRegExp rx { "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}" };

    QLineEdit *currentIP = new QLineEdit;
    currentIP->setValidator(new QRegExpValidator(rx));
    currentIP->setAlignment(Qt::AlignCenter);
    currentIP->setText(ip.toString());
    layout->addRow(trUtf8("Adresse"), currentIP);

    QPushButton *connection = new QPushButton;
    layout->addRow(trUtf8("Connecter/Déconnecter"), connection);
    connect(connection, &QPushButton::clicked, this, [=] () {
        const ConnectionStatus status = this->m_endatPtr->connectionStatus();
        if (status == ConnectionStatus::Connected) {
            this->m_endatPtr->stop();
        }
        else {
            this->m_endatPtr->setEibAddress(QHostAddress(currentIP->text()).toIPv4Address());
            this->m_endatPtr->startPollingMode();
        }
    });

    QLineEdit *ipStatus = new QLineEdit;
    ipStatus->setAlignment(Qt::AlignCenter);
    ipStatus->setReadOnly(true);
    ipStatus->setText(statusToString(m_endatPtr->connectionStatus()));
    layout->addRow(trUtf8("Status de la connexion"), ipStatus);



    QLabel *infos = new QLabel;
    infos->setStyleSheet("background-color: white");

    auto updateInfos = [this, infos] () {
        EibInfos eibInfos = this->m_endatPtr->eibInfos();
        HtmlTable table;
        table.addRow(trUtf8("Eib ID"), QString::fromStdString(eibInfos.id));
        table.addRow(trUtf8("Hostname"), QString::fromStdString(eibInfos.hostname));
        table.addRow(trUtf8("IP"), QHostAddress(eibInfos.network.ip).toString());
        table.addRow(trUtf8("Netmask"), QHostAddress(eibInfos.network.netmask).toString());
        table.addRow(trUtf8("Gateway"), QHostAddress(eibInfos.network.gateway).toString());
        table.addRow(trUtf8("Adresse MAC"), macToString(eibInfos.mac));
        table.addRow(trUtf8("DHCP"), eibInfos.network.dhcp?trUtf8("oui"):trUtf8("non"));
        table.addRow(trUtf8("Timeout DHCP"), QString::number(eibInfos.network.dhcpTimeout) + " s");
        table.addRow(trUtf8("Connexions ouvertes"), QString::number(eibInfos.numberOfOpenConnections));
        infos->setText(table);
    };

    updateInfos();
    layout->addRow(trUtf8("Infos Eib"), infos);


    { // réseau
        layout->addRow(new LineSeparator);

        EibInfos eibInfos = m_endatPtr->eibInfos();

        QCheckBox *activateNetworkModify = new QCheckBox;
        layout->addRow(trUtf8("Modification réseau"), activateNetworkModify);

        QLineEdit *ip = new QLineEdit;
        ip->setValidator(new QRegExpValidator(rx));
        ip->setText(QHostAddress(eibInfos.network.ip).toString());
        layout->addRow(trUtf8("Adresse"), ip);

        QLineEdit *netmask = new QLineEdit;
        netmask->setValidator(new QRegExpValidator(rx));
        netmask->setText(QHostAddress(eibInfos.network.netmask).toString());
        layout->addRow(trUtf8("Masque"), netmask);

        QLineEdit *gateway = new QLineEdit;
        gateway->setValidator(new QRegExpValidator(rx));
        gateway->setText(QHostAddress(eibInfos.network.gateway).toString());
        layout->addRow(trUtf8("Passerelle"), gateway);

        QCheckBox *dhcp = new QCheckBox;
        dhcp->setChecked(eibInfos.network.dhcp);
        layout->addRow(trUtf8("Activer DHCP"), dhcp);

        QSpinBox *timeout = new QSpinBox;
        timeout->setRange(0, 30);
        timeout->setValue(static_cast<int>(eibInfos.network.dhcpTimeout));
        layout->addRow(trUtf8("Timeout DHCP"), timeout);

        QPushButton *apply = new QPushButton(trUtf8("Appliquer"));
        layout->addWidget(apply);
        connect(apply, &QPushButton::clicked, [this, ip, netmask, gateway, dhcp, timeout, updateInfos] () {
            int ret = QMessageBox::warning(this, trUtf8("Eib7 Network"),
                                           trUtf8("La modification du réseau ne s'appliquera "
                                                  "qu'au prochain démarrage de l'Eib.\n"
                                                  "Êtes vous sûr de vouloir faire cela ?"),
                                           QMessageBox::Ok | QMessageBox::Cancel,
                                           QMessageBox::Cancel);
            if (ret != QMessageBox::Ok) {
                return;
            }

            this->m_endatPtr->setNetworkConfiguration(EibNetwork {QHostAddress(ip->text()).toIPv4Address(),
                                                                  QHostAddress(netmask->text()).toIPv4Address(),
                                                                  QHostAddress(gateway->text()).toIPv4Address(),
                                                                  dhcp->isChecked(),
                                                                  static_cast<uint32_t>(timeout->value())});
            updateInfos();
        });

        auto enableWidgets = [=] (int state) {
            const bool enabled = (state==Qt::Checked);
            ip->setEnabled(enabled);
            netmask->setEnabled(enabled);
            gateway->setEnabled(enabled);
            dhcp->setEnabled(enabled);
            timeout->setEnabled(enabled);
            apply->setEnabled(enabled);
        };

        enableWidgets(Qt::Unchecked);

        connect(activateNetworkModify, &QCheckBox::stateChanged, this, enableWidgets);

        layout->addRow(new LineSeparator);


    }

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
    connect(buttonBox, &QDialogButtonBox::accepted, this, [=] () {
        QSettings settings;
        settings.beginGroup("Eib741");
        settings.setValue("Address", QHostAddress(m_endatPtr->eibAddress()).toString());
        settings.endGroup();
    });
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    layout->addRow(buttonBox);


    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [=] () {
        const ConnectionStatus status = this->m_endatPtr->connectionStatus();
        ipStatus->setText(statusToString(status));
        if (status == ConnectionStatus::Connected) {
            connection->setText(trUtf8("Déconnecter"));
        }
        else {
            connection->setText(trUtf8("Connecter"));
        }
    });
    timer->start(500);

    setLayout(layout);
}

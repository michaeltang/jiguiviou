/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtreticle.h"
#include "gvspqtreticle_p.h"

#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLShaderProgram>

using namespace Jgv::Gvsp::QtBackend;

Reticle::Reticle()
    : GlEffect(*new ReticlePrivate)
{}

Reticle::~Reticle() = default;

void Reticle::initialize(QOpenGLContext *context, const GLQuadBuffers &quad)
{
    IMPL(Reticle);
    GlEffect::initialize(context, quad);

    std::array<GLfloat, 18 * 4> vertices {
        {        0.f,      1.f,      0.f,     -1.f,
                -1.f,      0.f,      1.f,      0.f,

            -4.f/8.f,      1.f,  4.f/8.f,      1.f,
            -3.f/8.f,  3.f/4.f,  3.f/8.f,  3.f/4.f,
            -2.f/8.f,  2.f/4.f,  2.f/8.f,  2.f/4.f,
            -1.f/8.f,  1.f/4.f,  1.f/8.f,  1.f/4.f,
            -4.f/8.f,     -1.f,  4.f/8.f,     -1.f,
            -3.f/8.f, -3.f/4.f,  3.f/8.f, -3.f/4.f,
            -2.f/8.f, -2.f/4.f,  2.f/8.f, -2.f/4.f,
            -1.f/8.f, -1.f/4.f,  1.f/8.f, -1.f/4.f,

                 1.f, -4.f/8.f,      1.f,  4.f/8.f,
             3.f/4.f, -3.f/8.f,  3.f/4.f,  3.f/8.f,
             2.f/4.f, -2.f/8.f,  2.f/4.f,  2.f/8.f,
             1.f/4.f, -1.f/8.f,  1.f/4.f,  1.f/8.f,
                -1.f, -4.f/8.f,     -1.f,  4.f/8.f,
            -3.f/4.f, -3.f/8.f, -3.f/4.f,  3.f/8.f,
            -2.f/4.f, -2.f/8.f, -2.f/4.f,  2.f/8.f,
            -1.f/4.f, -1.f/8.f, -1.f/4.f,  1.f/8.f }

    };

    d->reticleProgramPtr.reset(new QOpenGLShaderProgram);
    d->reticleProgramPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
    d->reticleProgramPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/primitivedraw.frag");
    d->reticleProgramPtr->link();

    d->reticleProgramPtr->bind();
    d->reticleProgramPtr->setUniformValue("color", QVector4D(0.8f, 0.8f, 0.8f, 0.5f));
    d->reticleProgramPtr->release();

    d->reticleVbo.create();
    d->reticleVbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    d->reticleVbo.bind();
    d->reticleVbo.allocate(vertices.data(), vertices.size() * sizeof(GLfloat));
    d->reticleVbo.release();

    d->reticleVao.create();
    d->reticleVao.bind();
    d->reticleVbo.bind();
    d->reticleProgramPtr->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    d->reticleProgramPtr->enableAttributeArray(0);
    d->reticleVao.release();

}

void Reticle::changeGeometry(const QSize &size)
{
    Q_UNUSED(size)
}

void Reticle::destroy()
{
    IMPL(Reticle);

    if (d->reticleProgramPtr) {
        d->reticleProgramPtr->removeAllShaders();
    }
    if (d->reticleVao.isCreated()) {
        d->reticleVao.destroy();
    }
    if (d->reticleVbo.isCreated()) {
        d->reticleVbo.destroy();
    }
}

void Reticle::render(const GLQuadRendering &fbo, const PboMetas &metas)
{
    Q_UNUSED(metas)
    IMPL(Reticle);

    if (d->config->overlayReticuleIsActive()) {
        d->reticleProgramPtr->bind();
        d->functions->glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo.fbo);
        d->functions->glEnable(GL_BLEND);
        d->functions->glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        d->functions->glBlendEquation (GL_FUNC_ADD);
        d->reticleVao.bind();
        d->functions->glDrawArrays(GL_LINES, 0, 36);
        d->reticleVao.release();
        d->functions->glDisable(GL_BLEND);
        d->functions->glBindFramebuffer(GL_FRAMEBUFFER, 0);
        d->reticleProgramPtr->release();
    }
}

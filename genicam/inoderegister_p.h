/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef REGISTERINODE_P_H
#define REGISTERINODE_P_H

#include "inode_p.h"
#include "pvalue.h"
#include <QVector>

namespace Jgv {

namespace GenICam {

namespace IntSwissKnife {
class Object;
}

namespace InodeRegister {

namespace Endianess {
constexpr const char * BigEndian = "BigEndian";
constexpr const char * LittleEndian = "LittleEndian";
}

namespace Cache {
constexpr const char * NoCache = "NoCache";              // pas de cache
constexpr const char * WriteThrough = "WriteThrough";    // une écriture sur le device implique une écriture sur le cache
constexpr const char * WriteAround = "WriteAround";      // seule la lecture est mise en cache
}

namespace Properties {
constexpr const char * Address = "Address";
constexpr const char * Length = "Length";
constexpr const char * pLength = "pLength";
constexpr const char * AccessMode = "AccessMode";
constexpr const char * pPort = "pPort";
constexpr const char * Cachable = "Cachable";
constexpr const char * PollingTime = "PollingTime";
constexpr const char * pAddress = "pAddress";
constexpr const char * IntSwissKnife = "IntSwissKnife";
constexpr const char * pInvalidator = "pInvalidator";
constexpr const char * pIndex = "pIndex";
constexpr const char * Endianess = "Endianess";
}

namespace Attributes {
constexpr const char * Offset = "Offset";
constexpr const char * pOffset = "pOffset";
}

struct Index
{
    pValueInteger pValue;
    pValueInteger pOffset;
    qint64 value() const { return pValue.value() * pOffset.value(); }
};

class ObjectPrivate : public Inode::ObjectPrivate
{
    using get = qint64 (ObjectPrivate::*) () const;

public:
    ObjectPrivate(QSharedPointer<IPort::Interface> iport);

    QList<qint64> addresses;
    QList<pValueInteger> pAddresses;
    QList<QSharedPointer<IntSwissKnife::Object> > intSwissKnives;
    QList<Index> pIndexes;

    pValueInteger pLength;

    bool canRead = false;
    bool canWrite = false;
    bool readUpdateCache = true;
    bool writeUpdateCache = true;
    bool isLittleEndian = false;

    QString pPort;
    int pollingTime = -1;

    QList<QWeakPointer<Inode::Object> > invalidatorsList;
    QVector<quint8> data;
};

} // namespace InodeRegister

} // namespace GenICam

} // namespace Jgv

#endif // REGISTERINODE_P_H

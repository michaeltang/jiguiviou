/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "externsync.h"
#include "externsync_p.h"

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <vector>
#include <boost/endian/conversion.hpp>

using namespace ExternSync;
using namespace boost::endian;

const std::size_t BUFFER_SIZE = 256;

namespace {

void receivepackets(ExternSync::SocketPrivate *d)
{
    // préparation du polling sur le descripteur de socket
    pollfd pfd {d->sd,POLLIN,0};
    sockaddr_in from = {};
    socklen_t fromSize = sizeof(from);
    std::vector<char> buffer(BUFFER_SIZE, 0);

    while (d->run) {
        // attend un évènement à lire
        int pollResult = poll(&pfd, 1, 500);
        if (pollResult > 0) {   // oki, on lit
            const ssize_t read = recvfrom(d->sd, buffer.data(), BUFFER_SIZE, MSG_DONTWAIT, reinterpret_cast<sockaddr *>(&from), &fromSize);
            if (read > 0) {
                if (read == 1) {
                    std::clog << "ExternSync: bad received packet size";
                }
                for (auto const &observer : d->observers) {
                    switch (buffer[3]) {
                    case '0':
                        observer->changeSyncMode(Mode::M50Hz);break;
                    case '1':
                        observer->changeSyncMode(Mode::M25Hz_EVEN);break;
                    case '2':
                        observer->changeSyncMode(Mode::M25Hz_ODD);break;
                    }
                }
            }
            else if (read < 0) {
                std::perror("ExternSync: failed to receive packet");
            }
            else {
                std::clog << "ExternSync: 0 byte readed" << std::endl;
            }
        }
    }
}
}


Socket::Socket()
    : p_impl(new SocketPrivate)
{}

Socket::~Socket()
{
    IMPL(Socket);
    if (d->threadPtr && d->threadPtr->joinable()) {
        d->run = false;
        d->threadPtr->join();
    }
}

void Socket::addObserver(std::shared_ptr<Observer> observer)
{
    IMPL(Socket);
    d->observers.push_back(observer);
}


void Socket::listen()
{
    IMPL(Socket);
    // descripteur du socket UDP
    d->sd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if ( d->sd < 0 ) {
        std::perror("ExternSync: failed to create socket");
        return;
    }

    // bind du socket UDP
    sockaddr_in localAddress = {};
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = native_to_big<uint16_t>(0);   // port aléatoire
    localAddress.sin_addr.s_addr = native_to_big<uint32_t>(0);


    if ( bind(d->sd, reinterpret_cast<sockaddr *>(&localAddress), sizeof(struct sockaddr_in) ) == -1 ) {
        std::perror("ExternSync: failed to bind socket");
        return;
    }

    d->threadPtr = std::unique_ptr<std::thread>(std::move(new std::thread(&receivepackets, d)));
    pthread_setname_np(d->threadPtr->native_handle(), "ExternSync");
}

void Socket::setMode(Mode mode)
{
    IMPL(Socket);

    if (d->sd < 0) {
        std::clog << "ExternSync: failed tio change mode, bad socket" << std::endl;
        return;
    }

    sockaddr_in dest = {};
    dest.sin_family = AF_INET;
    dest.sin_port = native_to_big(d->port);
    dest.sin_addr.s_addr = native_to_big(d->ip);
    socklen_t destlen = sizeof(dest);

    auto list = {'S', 'E', '0', 'F'};
    std::vector<char> cmd { list };

    switch (mode) {
    case Mode::M50Hz:
        cmd[2] = '0';break;
    case Mode::M25Hz_EVEN:
        cmd[2] = '1';break;
    case Mode::M25Hz_ODD:
        cmd[2] = '2';break;
    case Mode::CURRENT:
        cmd[2] = '?';break;
    }

    // expédie sans bloquer
    if  (sendto(d->sd, cmd.data(), cmd.size(), 0/*MSG_DONTWAIT*/, reinterpret_cast<const sockaddr *>(&dest), destlen) == -1) {
        std::perror("ExternSync: failed to send cmd");
    }
}





/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPRECEIVER_H
#define GVSPRECEIVER_H


#include "gvspmemoryallocator.h"

#include <memory>
#include "pimpl.hpp"

#ifdef ENDAT
class Endat;
#endif

namespace Jgv
{

namespace Gvsp
{

enum class SocketType
{
    NoType,
    Classic,
    RingBuffer,
};

enum class TimestampSource : int
{
    GEVTransmitter = 0,
    TimestampDate = 1,
    SystemClock = 2
};

struct Geometry
{
    uint32_t width;
    uint32_t height;
    uint32_t pixelFormat;
    inline bool operator !=(const Geometry &other) const noexcept {return (other.width!=width)||(other.height!=height)||(other.pixelFormat!=pixelFormat);}
    inline bool operator ==(const Geometry &other) const noexcept {return (other.width==width)&&(other.height==height)&&(other.pixelFormat==pixelFormat);}
};

struct Image
{
    Geometry geometry;
    std::size_t geometrySize;
    uint64_t timestamp;
#ifdef ENDAT
    int32_t site;
    int32_t gisement;
#endif
    std::size_t dataSize;
    Jgv::Gvsp::MemoryBlock memory;
};

struct ReceiverStatistics
{
    uint64_t imagesCount;
    uint64_t imagesLostCount;
    uint64_t segmentsResendCount;
    uint64_t segmentsLostCount;
    uint64_t lastTimestamp;
};

struct Parameters {
    uint32_t receiverIP;
    uint32_t transmitterIP;
    uint32_t multicastGroup;
    uint16_t receiverPort;
    uint16_t transmitterPort;
    SocketType socketType;
    TimestampSource timestampSrc;
    uint64_t timestampFrequency;
    bool resend;
    bool isMulticast;
    Geometry geometry;
};

class MemoryAllocator;
class ReceiverPrivate;
class Receiver
{

public:
    Receiver();
    explicit Receiver(MemoryAllocator *allocator);
protected:
    Receiver(ReceiverPrivate &dd);
public:
    virtual ~Receiver();

    void listenUnicast(uint32_t bindAddress);
    void listenMulticast(uint32_t bindAddress, uint32_t multicastAddress);
    void acceptFrom(uint32_t transmitterIP, uint16_t transmitterPort);
    void preallocImages(const Geometry &geometry, uint32_t packetSize);
    void setTimestampSource(TimestampSource source);
    void setTransmitterTimestampFrequency(uint64_t frequency);
    void setResendActive(bool active);
    void pushDatation(uint64_t timestamp, uint64_t dateMin, uint64_t dateMax);

    const Parameters &parameters() const;
    const ReceiverStatistics & statistics() const;

#ifdef ENDAT
    std::weak_ptr<Endat> getEndat();
#endif

protected:
    MemoryAllocator &allocator();
    const MemoryAllocator &allocator() const;

private:
    BASE_PIMPL(Receiver)


}; // class Receiver

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPRECEIVER_H

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "register.h"
#include "register_p.h"
#include "genicamobjectbuilder.h"
#include "xmlhelper.h"
#include "iinteger.h"
#include "iport.h"

#include <QtEndian>
#include <QStringList>
#include <QVariant>
#include <cstring>


using namespace Jgv::GenICam::Register;

ObjectPrivate::ObjectPrivate(QSharedPointer<Jgv::GenICam::IPort::Interface> iport)
    : InodeRegister::ObjectPrivate(iport)
{}


Object::Object(QSharedPointer<IPort::Interface> iport)
    : InodeRegister::Object(*new ObjectPrivate(iport))
{}

void Object::prepare(InterfaceBuilder &builder, const XMLHelper &helper)
{
    Q_D(Object);

    InodeRegister::Object::prepare(builder, helper);
    d->sType = sType;
}

void Object::invalidate()
{
    Q_D(Object);

    d->cache.isValid = false;
}

QVariant Object::getVariant()
{
    return QVariant(QByteArray(reinterpret_cast<const char*>(data()), static_cast<int>(dataSize())));
}

void Object::get(quint8 *buffer, qint64 length)
{
    const uchar *p = data();
    if (p != nullptr) {
        if ((buffer!=nullptr) && (length<=dataSize())) {
            std::memcpy(buffer, data(), static_cast<std::size_t>(length));
        }
    }
    else {
        qWarning("Register %s, InodeRegister pointer failed !", qPrintable(featureName()));
    }

}

void Object::set(quint8 *buffer, qint64 length)
{
    qint64 size = dataSize();
    if (buffer && length <= size) {
        uchar *p = data();
        std::fill(p, p + size, 0);
        std::memcpy(p, buffer, static_cast<std::size_t>(length));
        updateData();
    }
}

qint64 Object::getAddress()
{
    return InodeRegister::Object::address();
}

qint64 Object::getLength()
{
    return dataSize();
}

Interface *Object::interface()
{
    return this;
}

const Interface *Object::constInterface() const
{
    return this;
}






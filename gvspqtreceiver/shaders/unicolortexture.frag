#version 420 core
layout(binding = 0) uniform sampler2DRect tex;
layout(location = 0) out vec4 outColor;
uniform vec4 color;
uniform int var;

in vec2 vTexCoord;

void main()
{
    outColor = texture(tex, vTexCoord).rrrr * color;
}

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "firstpage.h"
#include "firstpage_p.h"

#include "secondpage.h"

using namespace Jgv::GLContainer;

FirstPage::FirstPage()
    : Page( *new FirstPagePrivate)
{
    IMPL(FirstPage);

    auto button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Record);
    button->gridRect.moveTopLeft({0,0});
    auto notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setJpegIsRecording(!config->jpegIsRecording());
        }
    });
    d->widgets.emplace_back( std::move(button) );


    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Equalization);
    button->gridRect.moveTopLeft({0,2});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [d]() {
        auto config = d->config.toStrongRef();
        if (config) {
            config->setHistogramEqIsActive(!config->histogramEqIsActive());
        }
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::More);
    button->gridRect.moveTopLeft({0,4});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    auto child = std::make_shared<SecondPage>();
    d->childs.emplace_back(child);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [this, child]() {
        this->setVisible(false);
        child->setParent(this);
        child->setVisible(true);
    });
    d->widgets.emplace_back( std::move(button) );

    button = std::make_unique<GLWidget::Button>();
    button->setButtonType(GLWidget::ButtonType::Return);
    button->gridRect.moveTopLeft({0,5});
    notifier = std::make_shared<NotifyWrapper>();
    button->setNotify(notifier);
    QObject::connect(notifier.get(), &NotifyWrapper::onClicked, [this]() {
        this->setVisible(false);
    });
    d->widgets.emplace_back( std::move(button) );

}

FirstPage::~FirstPage() = default;













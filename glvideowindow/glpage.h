/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGE_H
#define GLPAGE_H

#include <pimpl.hpp>
#include <vector>

#include "glwidget.h"

namespace Jgv {

namespace Gvsp {
namespace QtBackend {
class ReceiverConfig;
}
}

namespace GLContainer {

class PagePrivate;
class Page
{

protected:
    Page(PagePrivate &dd);
public:
    virtual ~Page();

    void setParent(Page *parent);

    void setReceiverConfig(QWeakPointer<Jgv::Gvsp::QtBackend::ReceiverConfig> config);
    void setViewSize(int w, int h);

    bool isVisible() const;
    void setVisible(bool visible);

    bool haveChilds() const;
    const std::vector<std::shared_ptr<Page>> &childs() const;

    const std::vector<std::unique_ptr<GLWidget::Widget>> &widgets() const;

    bool event(QEvent *event);

private:
    BASE_PIMPL(Page)
}; // class Page

} // namespace GLContainer;

} // namespace Jgv

#endif // GLPAGE_H

/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "geviport.h"
#include <bootstrapregisters.h>
#include <writereghelper.h>

using namespace Jgv;

void GevIPort::addReceiver(uint channel, const uint32_t &address, uint16_t port)
{
    writeRegisters(
    { Jgv::Gvcp::ADDRESS_VALUE{Jgv::Gvcp::BootstrapRegisters::streamChannelTranspose(channel, Jgv::Gvcp::BootstrapAddress::StreamChannelDestinationAddress), address},
      Jgv::Gvcp::ADDRESS_VALUE{Jgv::Gvcp::BootstrapRegisters::streamChannelTranspose(channel, Jgv::Gvcp::BootstrapAddress::StreamChannelPort), port} }
                );
}

void GevIPort::read(uint8_t *pBuffer, int64_t address, int64_t length)
{
    const uint8_t *memory = readMemory(static_cast<uint32_t>(address), static_cast<uint16_t>(length));
    if (memory == nullptr) {
        //qDebug("GevIport failed to read address 0x%llX size %lld", static_cast<int64_t>(address), length);
        return;
    }
    std::memcpy(pBuffer, memory, static_cast<std::size_t>(length));

}

void GevIPort::write(uint8_t *pBuffer, int64_t address, int64_t length)
{
    if (!writeMemory(static_cast<uint32_t>(address), pBuffer, static_cast<uint>(length))) {
        //qDebug("GevIport failed to write address 0x%llX size %lld", static_cast<int64_t>(address), length);
    }
}

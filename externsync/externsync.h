/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef EXTERNSYNC_H
#define EXTERNSYNC_H

#include <pimpl.hpp>

namespace ExternSync {

enum class Mode {
    M50Hz, M25Hz_EVEN, M25Hz_ODD,
    CURRENT
};

class Observer {
public:
    virtual ~Observer() = default;
    virtual void changeSyncMode(Mode newMode) = 0;
};

class SocketPrivate;
class Socket {
public:
    Socket();
    ~Socket();

    void addObserver(std::shared_ptr<Observer> observer);
    void listen();
    void setMode(Mode mode);


private:
    BASE_PIMPL(Socket)

};

} // namespace ExternSync

#endif // EXTERNSYNC_H

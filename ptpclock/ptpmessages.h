/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PTPMESSAGES_H
#define PTPMESSAGES_H

#include "ptpderivedtypes.h"
#include "ptpmessageheader.h"

#include <QVector>

namespace PTP {

class AnnounceMessage : public MessageHeader
{
public:
    AnnounceMessage(char *data);
    DerivedTypes::Timestamp originTimestamp() const;
    qint16 currentUtcOffset() const;
    quint8 grandmasterPriority1() const;
    DerivedTypes::ClockQuality grandmasterClockQuality() const;
    quint8 grandmasterPriority2() const;
    DerivedTypes::ClockIdentity grandmasterIdentity() const;
    quint16 stepsRemoved() const;
    qint8 timeSource() const;
    static int size();
};

namespace Management {
namespace Action {
enum Value {
    GET = 0x00,
    SET = 0x01,
    RESPONSE = 0x02,
    COMMAND = 0x03,
    ACKNOWLEDGE = 0x04
};
}

namespace Id {
enum Value {
    NullManagement = 0x0000,
    ClockDescription = 0x0001,
    UserDescription = 0x0002
};
}




class ManagementTlv : public DerivedTypes::Tlv
{

public:
    ManagementTlv(quint8 *data);
    ManagementTlv(int dataFieldSize);

    Id::Value managementId() const;
    void setManagementId(Id::Value id);

    quint8 *dataField() const;

    static int nearEven(int val);
};

class ManagementMessage : public MessageHeader
{
public:
    ManagementMessage(char *data);
    ManagementMessage(Management::Action::Value action, const ManagementTlv &tlv);

    DerivedTypes::PortIdentity targetPortIdentity();

    quint8 startingBoudaryHops() const;
    void setStartingBoudaryHops(quint8 hops);

    quint8 boudaryHops() const;
    void setBoudaryHops(quint8 hops);

    Management::Action::Value actionField() const;
    void setActionField(Management::Action::Value action);

    ManagementTlv managementTlv() const;

};

} // Message

} // Ptp


#endif // PTPMESSAGES_H

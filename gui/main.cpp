/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QLibraryInfo>

#include "mainwidget.h"

int main(int argc, char *argv[])
{
#ifdef QTRECEIVER
    // on va utiliser le partage de context OpenGL
    QApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
#endif
    QApplication::setOrganizationName("Baletaud");
    QApplication::setApplicationName("Jiguiviou");

    QApplication app(argc, argv);

    //    QString locale = QLocale::system().name().section('_', 0, 0);
    //    QTranslator translator;
    //    translator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    //    app.installTranslator(&translator);



    //setenv("GST_DEBUG", "3", 1);

    bool fullscreen = false;
    for (int i = 1; i < argc; ++i)
        if (!qstrcmp(argv[i], "--fullscreen"))
            fullscreen = true;

    Jiguiviou::MainWidget widget;
    if (fullscreen)
        widget.showFullScreen();
    else
        widget.show();

    return app.exec();
}

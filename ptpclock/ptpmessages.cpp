/***************************************************************************
 *   Copyright (C) 2014-2016 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "ptpmessages.h"

#include <QtEndian>

const int MANAGEMENT_STATIC_SIZE = 14;

using namespace PTP;
using namespace PTP::Management;

AnnounceMessage::AnnounceMessage(char *data)
    : MessageHeader(data)
{}

DerivedTypes::Timestamp AnnounceMessage::originTimestamp() const
{
    return DerivedTypes::Timestamp(m_me + 34);
}

qint16 AnnounceMessage::currentUtcOffset() const
{
    return qFromBigEndian<qint16>(m_me + 44);
}

quint8 AnnounceMessage::grandmasterPriority1() const
{
    return m_me[47];
}

DerivedTypes::ClockQuality AnnounceMessage::grandmasterClockQuality() const
{
    return DerivedTypes::ClockQuality(m_me + 48);
}

quint8 AnnounceMessage::grandmasterPriority2() const
{
    return m_me[52];
}

DerivedTypes::ClockIdentity AnnounceMessage::grandmasterIdentity() const
{
    return DerivedTypes::ClockIdentity(m_me + 53);
}

quint16 AnnounceMessage::stepsRemoved() const
{
    return qFromBigEndian<quint16>(m_me + 61);
}

qint8 AnnounceMessage::timeSource() const
{
    return static_cast<qint8>(m_me[63]);
}

int AnnounceMessage::size()
{
    return 64;
}

ManagementTlv::ManagementTlv(quint8 *data)
    : DerivedTypes::Tlv(data)
{}

ManagementTlv::ManagementTlv(int dataFieldSize)
    : DerivedTypes::Tlv(6 + dataFieldSize)
{
    qToBigEndian<quint16>(static_cast<qint16>(TlvType::Management), me);
    qToBigEndian<quint16>(static_cast<qint16>(2 + dataFieldSize), me + 2);
}

Id::Value ManagementTlv::managementId() const
{
    return static_cast<Id::Value>(qFromBigEndian<qint16>(me + 4));
}

void ManagementTlv::setManagementId(Id::Value id)
{
    qToBigEndian<qint16>(static_cast<qint16>(id), me + 4);
}

quint8 *ManagementTlv::dataField() const
{
    return me + 6;
}

int ManagementTlv::nearEven(int val)
{
    return (val%2==0)?val:1+val;
}

ManagementMessage::ManagementMessage(char *data)
    : MessageHeader(data)
{}

ManagementMessage::ManagementMessage(Action::Value action, const ManagementTlv &tlv)
    : MessageHeader(MessageHeader::size() + MANAGEMENT_STATIC_SIZE + tlv.size())
{
    setMessageType(MessageType::Management);
    setVersionPTP(2);
    setMessageLength(MessageHeader::size() + MANAGEMENT_STATIC_SIZE + tlv.size());
    setDomainNumber(0);
    sourcePortIdentity().setPortNumber(1);
    setControlField(ControlField::Management);
    setLogMessageInterval(0x7F);
    targetPortIdentity().clockIdentity().setIdentity(QVector<quint8>(8, 0xFF));
    targetPortIdentity().setPortNumber(0xFFFF);
    setStartingBoudaryHops(1);
    setBoudaryHops(0);
    setActionField(action);
    ::memcpy(m_me + 48, tlv.data(), tlv.size());
}

PTP::DerivedTypes::PortIdentity ManagementMessage::targetPortIdentity()
{
    return DerivedTypes::PortIdentity(m_me + 34);
}

quint8 ManagementMessage::startingBoudaryHops() const
{
    return m_me[44];
}

void ManagementMessage::setStartingBoudaryHops(quint8 hops)
{
    m_me[44] = hops;
}

quint8 ManagementMessage::boudaryHops() const
{
    return m_me[45];
}

void ManagementMessage::setBoudaryHops(quint8 hops)
{
    m_me[45] = hops;
}

Action::Value ManagementMessage::actionField() const
{
    return static_cast<Action::Value>(m_me[46] & 0x0F);
}

void ManagementMessage::setActionField(Action::Value action)
{
    m_me[46] = 0x0F & static_cast<quint8>(action);
}

ManagementTlv ManagementMessage::managementTlv() const
{
    return ManagementTlv(reinterpret_cast<quint8 *>(m_me + 48));
}













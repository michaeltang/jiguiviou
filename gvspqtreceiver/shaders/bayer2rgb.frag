#version 420 core

layout(binding = 0) uniform sampler2DRect tex;
layout(location = 0) out vec3 outColor;
in vec4 center;
in vec4 yCoord;
in vec4 xCoord;


float fetch(vec2 xy) {
    return texelFetch(tex, ivec2(xy)).r;
}

float fetch(float x, float y) {
    return texelFetch(tex, ivec2(x, y)).r;
}

const vec4 kA = vec4(-1.0, -1.5,  0.5, -1.0) / 8.0;
const vec4 kB = vec4( 2.0,  0.0,  0.0,  4.0) / 8.0;
const vec4 kC = vec4( 4.0,  6.0,  5.0,  5.0) / 8.0;
const vec4 kD = vec4( 0.0,  2.0, -1.0, -1.0) / 8.0;
const vec4 kE = vec4(-1.0, -1.5, -1.0,  0.5) / 8.0;
const vec4 kF = vec4( 2.0,  0.0,  4.0,  0.0) / 8.0;

void main(void) {
    float C = fetch(center.xy);
    vec2 alternate = mod(center.zw, 2.0);

    // obtient les 4 valeurs autour du pixel
    vec4 Dvec = vec4(
                fetch(xCoord.y, yCoord.y),
                fetch(xCoord.y, yCoord.z),
                fetch(xCoord.z, yCoord.y),
                fetch(xCoord.z, yCoord.z));
    // somme les 4 valeurs dans x
    Dvec.xy += Dvec.zw;
    Dvec.x  += Dvec.y;


    vec4 value = vec4(
                fetch(center.x, yCoord.x),
                fetch(center.x, yCoord.y),
                fetch(xCoord.x, center.y),
                fetch(xCoord.y, center.y));

    value += vec4(
                fetch(center.x, yCoord.w),
                fetch(center.x, yCoord.z),
                fetch(xCoord.w, center.y),
                fetch(xCoord.z, center.y));

    vec4 PATTERN = vec4(kC.xyz * C, 0.0).xyzz;
    PATTERN.yzw += vec3(kD.yz * Dvec.x, 0.0).xyy;
    PATTERN += vec4(kA.xyz * value.x, 0.0).xyzx + vec4(kE.xyw * value.z, 0.0).xyxz;
    PATTERN.xw  += kB.xw * value.y;
    PATTERN.xz  += kF.xz * value.w;

    outColor = (alternate.y < 1.0) ?
                ((alternate.x < 1.0) ?
                     vec3(C, PATTERN.xy) :
                     vec3(PATTERN.z, C, PATTERN.w)) :
                ((alternate.x < 1.0) ?
                     vec3(PATTERN.w, C, PATTERN.z) :
                     vec3(PATTERN.yx, C));
}


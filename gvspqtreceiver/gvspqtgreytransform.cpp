/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtgreytransform.h"
#include "gvspqtgreytransform_p.h"

#include "gvsp.h"

#include "gvspqthistogramgetter.h"
#include "gvspqthistogrameequalization.h"
#include "gvspqtreticle.h"
#include "gvspqttimecodepainter.h"
#include "gvspqtjpegrecorder.h"
#include "gvspqthistogrampainter.h"
#include "gvspqtstateoverlay.h"
#include "gvspqtrectanglemask.h"
#include "gvspqtreceiverconfig.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLFramebufferObject>
#include <QOpenGLTexture>

using namespace Jgv::Gvsp::QtBackend;

void GreyTransformPrivate::populateEffects()
{
    effects.emplace(GlEffectType::HistogramGetter, std::make_unique<HistogramGetter>());
    effects.emplace(GlEffectType::HistogramEqualization, std::make_unique<HistogramEqualization>());
    effects.emplace(GlEffectType::Reticle, std::make_unique<Reticle>());
    effects.emplace(GlEffectType::TimecodePainter, std::make_unique<TimecodePainter>());
    effects.emplace(GlEffectType::JpegRecorder, std::make_unique<JpegRecorder>(JpegRecorderFormat::Grey));
    effects.emplace(GlEffectType::HistogramPainter, std::make_unique<HistogramPainter>());
    effects.emplace(GlEffectType::StateOverlay, std::make_unique<StateOverlay>());
    effects.emplace(GlEffectType::RectangleMask, std::make_unique<RectangleMask>());
    PixelTransformPrivate::populateEffects();
}

bool GreyTransformPrivate::initialize()
{
    if (PixelTransformPrivate::initialize()) {
        programPtr.reset( new QOpenGLShaderProgram );
        programPtr->addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
        programPtr->addShaderFromSourceFile(QOpenGLShader::Fragment, "://shaders/r2rgb.frag");
        programPtr->link();
        return true;
    }
    qCritical("Grey transform failed to initialize OpenGL resources");
    return false;
}

void GreyTransformPrivate::render(const PboMetas &pbo)
{
    auto resetMaskFbo = [this] (bool fromIn) {
        QOpenGLShaderProgram program;
        program.addShaderFromSourceFile(QOpenGLShader::Vertex, "://shaders/default.vert");
        if (fromIn) {
            program.addShaderFromSourceCode(QOpenGLShader::Fragment,
                                            "#version 420 core\n"
                                            "layout(binding = 0) uniform sampler2DRect tex;\n"
                                            "layout(location = 0) out vec4 outColor;\n"
                                            "in vec2 vTexCoord;\n"
                                            "void main()\n"
                                            "{ ivec2 coord = ivec2(vTexCoord);\n"
                                            "outColor = texelFetch(tex, coord).rrra; }\n");
            program.link();
            program.bind();
            inTexturePtr->bind();
            maskFboPtr->bind();
            vao.bind();
            functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
            vao.release();
            maskFboPtr->release();
            inTexturePtr->release();
            program.release();

        }
        else {
            program.addShaderFromSourceCode(QOpenGLShader::Fragment,
                                            "#version 420 core\n"
                                            "layout(location = 0) out vec4 outColor;\n"
                                            "void main()\n"
                                            "{ outColor = vec4(0.,0.,0.,0.); }\n");
            program.link();
            program.bind();
            maskFboPtr->bind();
            vao.bind();
            functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
            vao.release();
            maskFboPtr->release();
            program.release();
        }
    };



    context->makeCurrent(&surface);
    if (!fboPtr || (fboPtr->size() != QSize(pbo.width, pbo.height))) {
        changeGeometry({pbo.width, pbo.height});

        maskFboPtr.reset(new QOpenGLFramebufferObject(fboPtr->size(), fboPtr->format()) );
        resetMaskFbo(false);

        inTexturePtr.reset( new QOpenGLTexture(QOpenGLTexture::TargetRectangle) );
        inTexturePtr->setAutoMipMapGenerationEnabled(false);
        inTexturePtr->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
        inTexturePtr->setSize(pbo.width, pbo.height);
        inTexturePtr->setFormat(QOpenGLTexture::R8_UNorm);
        inTexturePtr->allocateStorage();

    }

    programPtr->bind();
    functions->glActiveTexture(GL_TEXTURE1);
    functions->glBindTexture(GL_TEXTURE_RECTANGLE, maskFboPtr->texture());

    functions->glActiveTexture(GL_TEXTURE0);
    inTexturePtr->bind();
    functions->glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo.id);
    functions->glTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, pbo.width, pbo.height, GL_RED, GL_UNSIGNED_BYTE, nullptr);
    functions->glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    if (config->blackLevelNeedUpdate()) {
        resetMaskFbo(config->blackLevelIsActive());
        config->setBlackLevelUpdated();
    }


    fboPtr->bind();
    //    functions->glClearColor(0.0, 1.0, 0.0, 0.0);
    //    functions->glClear(GL_COLOR_BUFFER_BIT);
    vao.bind();
    functions->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, reinterpret_cast<const GLvoid *>(0));
    vao.release();
    fboPtr->release();
    inTexturePtr->release();
    programPtr->release();

    const GLQuadRendering quad { fboPtr->handle(), inTexturePtr->textureId(), fboPtr->texture() };
    effects[GlEffectType::HistogramGetter]->render(quad, pbo);
    effects[GlEffectType::HistogramEqualization]->render(quad, pbo);
    effects[GlEffectType::Reticle]->render(quad, pbo);
    effects[GlEffectType::TimecodePainter]->render(quad, pbo);
    effects[GlEffectType::JpegRecorder]->render(quad, pbo);
    effects[GlEffectType::HistogramPainter]->render(quad, pbo);
    effects[GlEffectType::StateOverlay]->render(quad, pbo);
    effects[GlEffectType::RectangleMask]->render(quad, pbo);
    PixelTransformPrivate::render(pbo);
    context->doneCurrent();
}

void GreyTransformPrivate::destroy()
{
    context->makeCurrent(&surface);
    if (inTexturePtr) {
        inTexturePtr->destroy();
    }

    if (programPtr) {
        programPtr->removeAllShaders();
    }
    PixelTransformPrivate::destroy();
    context->doneCurrent();
}

GreyTransform::GreyTransform()
    : PixelTransform(*new GreyTransformPrivate)
{
    IMPL(GreyTransform);
    d->pixelFormats = PixelFormats {GVSP_PIX_MONO8};
}

GreyTransform::~GreyTransform() = default;




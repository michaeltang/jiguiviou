/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gllabel.h"
#include "gllabel_p.h"

#include <QEvent>
#include <QMouseEvent>
#include <QImage>
#include <QPainter>
#include <QSizeF>

using namespace Jgv::GLWidget;

void LabelPrivate::updateImage()
{
    WidgetPrivate::updateImage();
    QColor backgroud {Qt::darkGray};
    backgroud.setAlphaF(0.5);
    image.fill(Qt::transparent);
    QRectF textRect {rect};
    textRect.moveTopLeft( {0.,0.} );
    QPainter painter(&image);
    painter.setBrush(backgroud);
    painter.setPen(Qt::gray);
    painter.drawRect(0,0, image.width()-1, image.height()-1);
    QFont font;
    font.setPixelSize(qRound(rect.height() * 6. / (10. * linesCount)));
    textRect.moveTopLeft( {0., static_cast<qreal>(font.pixelSize()) / 2.0} );
    painter.setFont(font);
    painter.setPen(Qt::white);
    painter.drawText( textRect, Qt::AlignCenter, text);

}


Label::Label()
    : Widget(*new LabelPrivate)
{}
Label::~Label() = default;

void Label::setLines(int lineCount)
{
    IMPL(Label);
    d->linesCount = lineCount;
}


void Label::setString(const QString &string)
{
    IMPL(Label);
    if (d->text != string) {
        d->text = string;
        d->imageNeedUpdate = true;
    }
}








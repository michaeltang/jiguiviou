/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQRECTANGLEMASK_H
#define GVSPQRECTANGLEMASK_H

#include "gvspqtgleffect.h"

class QSize;

namespace Jgv {

namespace Gvsp {

namespace QtBackend {

class RectangleMaskPrivate;
class RectangleMask final : public GlEffect
{
public:
    RectangleMask();
    virtual ~RectangleMask() override final;

    virtual void initialize(QOpenGLContext *context, const GLQuadBuffers &quad) override final;
    virtual void changeGeometry(const QSize &size) override final;
    virtual void destroy() override final;
    virtual void render(const GLQuadRendering &fbo, const PboMetas &metas) override final;
private:
    PIMPL(RectangleMask)
};

} // namespace Qt

} // namespace Gvsp

} // namespace Jgv

#endif // GVSPQRECTANGLEMASK_H

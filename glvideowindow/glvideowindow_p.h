/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLVIDEOWINDOW_P_H
#define GLVIDEOWINDOW_P_H

#include "gvspqtdefinitions.h"
#include "swipeevent.h"
#include "glbutton.h"
#include "glkeyboard.h"
#include "gllabel.h"
#include "firstpage.h"
#include "secondpage.h"

#include <QScopedPointer>
#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLTexture>

class QOpenGLTexture;

namespace Jgv {

namespace Gvsp {
namespace QtBackend {
class ReceiverConfig;
}}

struct Keyboard
{
    std::unique_ptr<QOpenGLTexture> texture;
    GLWidget::Keyboard keyboard;
};

struct Label
{
    std::unique_ptr<QOpenGLTexture> texture;
    GLWidget::Label label;
};

class GLWindowWrapper;
class GLVideoWindowPrivate
{
public:
    QOpenGLFunctions_4_2_Core *functions = nullptr;
    QScopedPointer<QOpenGLShaderProgram> programPtr;
    QScopedPointer<QOpenGLShaderProgram> overlayProgramPtr;
    QOpenGLBuffer vbo { QOpenGLBuffer::VertexBuffer };
    QOpenGLBuffer indicesVbo { QOpenGLBuffer::IndexBuffer };
    QOpenGLVertexArrayObject vao;

    GLuint texture = 0;
    QVector2D textureSize {10., 10.};
    QMatrix4x4 transformTexture;
    QSharedPointer<GLWindowWrapper> videoWrapper;

    SwipeEvent swipe;


    volatile bool drawKeyboard = false;

    std::shared_ptr<GLContainer::Page> page = std::make_shared<GLContainer::FirstPage>();

    Keyboard keyboard;
    Label label;



    volatile bool textureSizeNeedUpdate = false;
    void updateTextureSize(const QSizeF &viewSize);

};

} // namespace Jiguiviou



#endif // GLVIDEOWINDOW_P_H
